<?php
class Superuser extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// check kredential
		$this->middleware->auth();

		// Print (array) Hak Akses Admin
		$this->data['privileges'] 			= $this->middleware->printAccess();

		$this->data['config'] 				= ConfigModel::find(1);
		$this->data['authmain'] 			= AuthModel::find($this->session->userdata('auth_id'));
		$this->data['notif_inbox'] 			= InboxModel::notReaded()->desc()->get();
		$this->data['notif_pricelist'] 		= PricelistModel::notReaded()->desc()->get();

		$this->data['notif_total'] 			= count($this->data['notif_inbox']) + count($this->data['notif_pricelist']);

		$this->blade->sebarno('ctrl', $this);
	}

	public function index($page="index")
	{
		$data 						= $this->data;


		$data['menu']				= "dashboard";
		echo $this->blade->nggambar('admin.dashboard',$data);
	}	

	// --------------------------------- START Admins
	public function admin($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('admin',true);

		$data['menu']				= "admin";
		$data['admin']				= AdminModel::desc()->get();

		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.admin.content',$data);	
			return;
		}
		else if ($url=="action" && $this->input->is_ajax_request() == true) {
			$rules 		= [
						    'required' 	=> [
						        ['action'],['data']
						    ]
						  ];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$action 	= $this->input->post('action');

			switch ($action) {
				case 'delete':
					$admin 	= AdminModel::whereIn('id',$this->input->post('data'))->get();

					AdminModel::whereIn('id',$this->input->post('data'))->delete();
			

					echo goResult(true,'Deleted!');
					break;
				
				default:
					echo goResult(false,"uknown action");
					break;
			}
		}
		else if($url=="checkusername" && $this->input->is_ajax_request()){
			$rules 		= [
				'required' 	=> [
					['username']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$check 				= AdminModel::where('username',$this->input->post('username'))->first();

			if($check){

				if($this->input->post('id')==$check->id){
					echo goResult(true,"username bisa di pakai");
					return;
				}

				echo goResult(false,"Opps! Username Telah Terpakai");
				return;
			}

			echo goResult(true,"username bisa di pakai");

			return;
		}
		else if($url=="changestatus" && $this->input->is_ajax_request()){
			$rules 		= [
				'required' 	=> [
					['status'],['id']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$admin				= AdminModel::find($this->input->post('id'));
			if(!$admin){
				echo goResult(false,"uknown id");
				return;
			}
			$admin->status 		= $this->input->post('status');
			$admin->save();
			echo goResult(true,"changed");

			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
						['name'],['ekskul'],['cp_admin'],['cp_pembina'],['nama_pembina'],['username'],['password'],['conf_password']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['password',8],['conf_password',8],['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			if($this->input->post('password')!==$this->input->post('conf_password')){
				echo goResult(false,"Password dan konfirmasi password tidak cocok");
				return;
			}

			$check 				= AdminModel::where('username',$this->input->post('username'))->first();

			if($check){
				echo goResult(false,"Opps! Username Telah Terpakai");
				return;
			}

			$admin				= new AdminModel;
	if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {

				$filename 	= 'LOGO__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/admin','logo',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$admin->logo= $upload['msg']['file_name'];
			}
			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'ADMIN__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/admin','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$admin->image= $upload['msg']['file_name'];
			}
			

			$admin->name				= $this->input->post('name');
			$admin->ekskul				= $this->input->post('ekskul');
			$admin->cp_admin			= $this->input->post('cp_admin');
			$admin->cp_pembina			= $this->input->post('cp_pembina');
			$admin->nama_pembina		= $this->input->post('nama_pembina');

			$admin->username			= $this->input->post('username');
			$admin->password 		= DefuseLib::encrypt($this->input->post('password'));
			$admin->ipaddress 		= $this->input->ip_address();
			$admin->lastlog 			= date('Y-m-d H:i:s ');
			$admin->status 			= (null !==$this->input->post('status') ? 'active' : 'blocked');
				if($admin->save()){
				echo goResult(true,"Akun Administrator Di Tambahkan");
				return;
			}
		}
		
		else if ($url=="update" && $id!=null){

			$data['admin'] 			= AdminModel::find($id);
			$data['type']			= "update";

			if(!isset($data['admin']->id)){
				redirect('superuser/admin');
				return;
			}

			echo $this->blade->nggambar('admin.admin.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$admin 		= AdminModel::find($id);

			if(!$admin){
				echo goResult(false,"Akun Admin Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['ekskul'],['cp_admin'],['cp_pembina'],['nama_pembina'],['username'],['password'],['conf_password']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['password',8],['conf_password',8],['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}


			if($this->input->post('password')!==$this->input->post('conf_password')){
				echo goResult(false,"Password dan konfirmasi password tidak cocok");
				return;
			}

			$check 				= AdminModel::where('username',$this->input->post('username'))->first();

			if($check){
				if($check->id!==$AdminModel->id){
					echo goResult(false,"Opps! Username Telah Terpakai");
					return;	
				}
			}
				if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {

				$filename 	= 'LOGO__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/admin','logo',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($AdminModel->logo!==""){
					remFile(__DIR__.'/../../public/images/admin/'.$AdminModel->logo);
				}

				$AdminModel->logo= $upload['msg']['file_name'];
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'ADMIN__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/admin','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($admin->image!==""){
					remFile(__DIR__.'/../../public/images/admin/'.$admin->image);
				}

				$admin->image= $upload['msg']['file_name'];
			}
			

			$admin->name				= $this->input->post('name');
			$admin->ekskul				= $this->input->post('ekskul');
			$admin->cp_admin			= $this->input->post('cp_admin');
			$admin->cp_pembina			= $this->input->post('cp_pembina');
			$admin->nama_pembina		= $this->input->post('nama_pembina');
			$admin->username			= $this->input->post('username');
			$admin->password 		= DefuseLib::encrypt($this->input->post('password'));
			$admin->ipaddress 		= $this->input->ip_address();
			$admin->lastlog 			= date('Y-m-d H:i:s ');
			$admin->status 			= (null !==$this->input->post('status') ? 'active' : 'blocked');
			if($admin->save()){
				echo goResult(true,"Akun Administrator Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){


			$auth 			= AuthModel::find($id);

			if(!$auth){
				redirect('superuser/auth');
				return;
			}

			AuthRuleModel::where('id_superuser',$auth->id)->delete();

			if($auth->image!=""){
				remFile(__DIR__.'/../../public/images/admin/'.$auth->image);
			}
			
			$auth->delete();

			redirect('superuser/admin');
		}
		else {
			echo $this->blade->nggambar('admin.admin.index',$data);	
			return;
		}
	}
// --------------------------------- END Admins

// --------------------------------- START Members
	public function member($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('member',true);

		$data['menu']				= "member";
		$data['member']				= MemberModel::desc()->get();

		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.member.content',$data);	
			return;
		}
		else if($url=="checkusername" && $this->input->is_ajax_request()){
			$rules 		= [
				'required' 	=> [
					['username']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$check 				= MemberModel::where('username',$this->input->post('username'))->first();

			if($check){

				if($this->input->post('id')==$check->id){
					echo goResult(true,"username bisa di pakai");
					return;
				}

				echo goResult(false,"Opps! Username Telah Terpakai");
				return;
			}

			echo goResult(true,"username bisa di pakai");

			return;
		}
		else if($url=="changestatus" && $this->input->is_ajax_request()){
			$rules 		= [
				'required' 	=> [
					['status'],['id']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$member				= MemberModel::find($this->input->post('id'));
			if(!$member){
				echo goResult(false,"uknown id");
				return;
			}
			$member->status 		= $this->input->post('status');
			$member->save();
			echo goResult(true,"changed");

			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
						['name'],['username'],['password'],['conf_password']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['password',8],['conf_password',8],['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			if($this->input->post('password')!==$this->input->post('conf_password')){
				echo goResult(false,"Password dan konfirmasi password tidak cocok");
				return;
			}

			$check 				= MemberModel::where('username',$this->input->post('username'))->first();

			if($check){
				echo goResult(false,"Opps! Username Telah Terpakai");
				return;
			}

			$member				= new MemberModel;
	if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {

				$filename 	= 'LOGO__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/member','logo',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$member->logo= $upload['msg']['file_name'];
			}
			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'MEMBER__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/member','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$member->image= $upload['msg']['file_name'];
			}
			

			$member->name				= $this->input->post('name');
			$member->username			= $this->input->post('username');
			$member->email				= $this->input->post('email');
			$member->password 			= DefuseLib::encrypt($this->input->post('password'));
			$member->ipaddress 			= $this->input->ip_address();
			$member->lastlog 			= date('Y-m-d H:i:s ');
			$member->status 			= (null !==$this->input->post('status') ? 'active' : 'blocked');
				if($member->save()){
				echo goResult(true,"Akun Members Di Tambahkan");
				return;
			}
		}
		
		else if ($url=="update" && $id!=null){

			$data['member'] 			= MemberModel::find($id);
			$data['type']			= "update";

			if(!isset($data['member']->id)){
				redirect('superuser/member');
				return;
			}

			echo $this->blade->nggambar('admin.member.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$member 		= MemberModel::find($id);

			if(!$member){
				echo goResult(false,"Akun Member Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['username'],['email'],['password'],['conf_password']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['password',8],['conf_password',8],['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}


			if($this->input->post('password')!==$this->input->post('conf_password')){
				echo goResult(false,"Password dan konfirmasi password tidak cocok");
				return;
			}

			$check 				= MemberModel::where('username',$this->input->post('username'))->first();

			if($check){
				if($check->id!==$MemberModel->id){
					echo goResult(false,"Opps! Username Telah Terpakai");
					return;	
				}
			}
				if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {

				$filename 	= 'LOGO__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/member','logo',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($MemberModel->logo!==""){
					remFile(__DIR__.'/../../public/images/member/'.$MemberModel->logo);
				}

				$MemberModel->logo= $upload['msg']['file_name'];
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'MEMBER__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/member','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($member->image!==""){
					remFile(__DIR__.'/../../public/images/member/'.$member->image);
				}

				$member->image= $upload['msg']['file_name'];
			}
			

			$member->name				= $this->input->post('name');
			$member->username			= $this->input->post('username');
			$member->email				= $this->input->post('email');
			$member->password 			= DefuseLib::decrypt ($this->input->post('password'));
			$member->ipaddress 			= $this->input->ip_address();
			$member->lastlog 			= date('Y-m-d H:i:s ');
			$member->status 			= (null !==$this->input->post('status') ? 'active' : 'blocked');
			if($member->save()){
				echo goResult(true,"Akun Members Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){


			$auth 			= AuthModel::find($id);

			if(!$auth){
				redirect('superuser/auth');
				return;
			}

			AuthRuleModel::where('id_superuser',$auth->id)->delete();

			if($auth->image!=""){
				remFile(__DIR__.'/../../public/images/member/'.$auth->image);
			}
			
			$auth->delete();

			redirect('superuser/member');
		}
		else {
			echo $this->blade->nggambar('admin.member.index',$data);	
			return;
		}
	}
// --------------------------------- END Members



// --------------------------------- START BLOG
	public function blog($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('blog',true);

		$data['menu']				= "blog";
		$data['category']			= CategoryBlogModel::desc()->get();
		$data['tag'] 				= TagModel::desc()->get();
		$data['blog']				= BlogModel::with('category')->desc()->get();


		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.blog.content',$data);	
			return;
		}

		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['name'],['description'],['category']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}
			$blog 				= new BlogModel;

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==false) {
				echo goResult(false,"Opss! Gambar Tidak Ada Atau Tidak Sesuai");
				return;
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'BLOG__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/blog','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$blog->image= $upload['msg']['file_name'];
			}
			

			$blog->name				= $this->input->post('name');
			$blog->description		= $this->input->post('description');
			$blog->status 			= (null !==$this->input->post('status') ? 0 : 1);

			$category 				= CategoryBlogModel::find($this->input->post('category'));

			if(!isset($category->id)){
				echo goResult(false,"Opps! Kategori Tidak Di Temukan");
			}
			$blog->id_category		= $category->id;

			if($blog->save()){
				$tag 			= $this->input->post('tag');
				if($tag){
					$tag 		= TagModel::whereIn('id',$tag)->desc()->get();
					foreach ($tag as $result) {
						$insertTag 				= new TagBlogModel;
						$insertTag->id_blog		= $blog->id;
						$insertTag->id_tag 		= $result->id;
						$insertTag->save();
					}
				}

				echo goResult(true,"Sukses! , Blog Baru Telah Di Tambahkan");
				return;
			}
		}
		else if ($url=="update" && $id!=null){

			$data['blog'] 			= BlogModel::with('category','tags')->find($id);

			$data['blogtag'] 		= [];

			foreach ($data['blog']->tags as $result) {
				array_push($data['blogtag'], $result->id_tag);
			}

			$data['type']			= "update";

			if(!isset($data['blog']->id)){
				redirect('superuser/blog');
				return;
			}

			echo $this->blade->nggambar('admin.blog.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$blog 					= BlogModel::find($id);

			if(!isset($blog->id)){
				echo goResult(false,"Opss! Blog Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['description'],['category']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'BLOG__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/blog/','image',$filename);

				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}

				if(!empty($upload['msg']['file_name'])){remFile(__DIR__.'/../../public/images/blog/'.$blog->image);}

				$blog->image = $upload['msg']['file_name'];
			}

			$blog->name				= $this->input->post('name');
			$blog->description		= $this->input->post('description');
			$blog->status 			= (null !==$this->input->post('status') ? 0 : 1);

			$category 				= CategoryBlogModel::find($this->input->post('category'));
			if(!isset($category->id)){
				echo goResult(false,"Opps! Kategori Tidak Di Temukan");
			}

			$blog->id_category		= $category->id;

			if($blog->save()){

				$tag 			= $this->input->post('tag');

				if($tag){

					TagBlogModel::where('id_blog',$blog->id)->delete();

					$tag 		= TagModel::whereIn('id',$tag)->desc()->get();
					foreach ($tag as $result) {
						$insertTag 				= new TagBlogModel;
						$insertTag->id_blog		= $blog->id;
						$insertTag->id_tag 		= $result->id;
						$insertTag->save();
					}
				}

				echo goResult(true,"Blog Telah Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){

			$blog 			= BlogModel::find($id);

			if(!isset($blog->id)){
				redirect('superuser/blog');
				return;
			}

			TagBlogModel::where('id_blog',$blog->id)->delete();

			if($blog->image!=""){
				if(file_exists("images/blog/{$blog->image}")){
					remFile(__DIR__.'/../../public/images/blog/'.$blog->image);
				}
			}
			
			$blog->delete();

			redirect('superuser/blog');
		}
		else {
			echo $this->blade->nggambar('admin.blog.index',$data);	
			return;
		}
	}
// --------------------------------- END BLOG


// --------------------------------- START BLOG KATEGORI 
	public function categoryblog($url=null,$id=null){
		$data 						= $this->data;
		$data['menu']				= "blog";
		$data['category']			= CategoryBlogModel::with('blog')->desc()->get();

		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.categoryblog.content',$data);	
			return;

		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules = [
					    'required' 	=> [
					        ['name'],['description']
					    ]
					  ];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$category 				= new CategoryBlogModel;

			$category->name 		= $this->input->post('name');
			$category->description 	= $this->input->post('description');

			if($category->save()){
				echo goResult(true,"Kategori Blog Telah Di Tambahkan");
				return;
			}

		}
		else if ($url=="update" && $id!=null){

			$data['category'] 		= CategoryBlogModel::find($id);
			$data['type']			= "update";

			if(!isset($data['category']->id)){
				redirect('superuser/categoryblog');
				return;
			}
			echo $this->blade->nggambar('admin.categoryblog.content',$data);	
			return;

		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$category 					= CategoryBlogModel::find($id);

			if(!isset($category->id)){
				echo goResult(false,"Opss! Kategori Tidak Di Temukan");
				return;
			}

			$rules = [
					    'required' 	=> [
					        ['name'],['description']
					    ]
					  ];


			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$category->name 		= $this->input->post('name');
			$category->description 	= $this->input->post('description');

			if($category->save()){
				echo goResult(true,"Kategori Telah Di Perbarui");
				return;
			}

		}
		else if ($url=="delete" && $id != null){

			$category 				= CategoryBlogModel::find($id);
			if(!isset($category->id)){
				redirect('superuser/categoryblog');
				return;
			}

			$blog 					= BlogModel::where('id_category',$category->id)->desc()->get();

			foreach ($blog as $result) {

				TagBlogModel::where('id_blog',$result->id)->delete();
				SliderModel::where('id_blog',$result->id)->delete();

				if($result->image!=""){
					if(file_exists("images/blog/{$result->image}")){
						remFile(__DIR__.'/../../public/images/blog/'.$result->image);
					}
				}
			}

			BlogModel::where('id_category',$category->id)->delete();

			$category->delete();
			redirect('superuser/categoryblog');
		}
		else {
			echo $this->blade->nggambar('admin.categoryblog.index',$data);	
			return;
		}
	}
// --------------------------------- END BLOG KATEGORI
// --------------------------------- START BLOG TAG
	public function tag($url=null,$id=null){
		$data 						= $this->data;
		$data['menu']				= "blog";
		$data['tag']				= TagModel::desc()->get();

		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.tag.content',$data);	
			return;

		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules = [
				'required' 	=> [
					['name'],['description']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$tag 				= new TagModel;

			$tag->name 			= $this->input->post('name');
			$tag->description 	= $this->input->post('description');

			if($tag->save()){
				echo goResult(true,"Tag Blog Telah Di Tambahkan");
				return;
			}

		}
		else if ($url=="update" && $id!=null){

			$data['tag'] 		= TagModel::find($id);
			$data['type']		= "update";

			if(!isset($data['tag']->id)){
				redirect('superuser/tag');
				return;
			}
			echo $this->blade->nggambar('admin.tag.content',$data);	
			return;

		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$tag 					= TagModel::find($id);

			if(!isset($tag->id)){
				echo goResult(false,"Opss! Tag Blog Tidak Di Temukan");
				return;
			}

			$rules = [
				'required' 	=> [
					['name'],['description']
				]
			];


			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$tag->name 			= $this->input->post('name');
			$tag->description 	= $this->input->post('description');

			if($tag->save()){
				echo goResult(true,"Tag Blog Telah Di Perbarui");
				return;
			}

		}
		else if ($url=="delete" && $id != null){

			$tag 				= TagModel::find($id);
			if(!isset($tag->id)){
				redirect('superuser/tag');
				return;
			}

			TagBlogModel::where('id_tag',$tag->id)->delete();
			$tag->delete();

			redirect('superuser/tag');
		}
		else {
			echo $this->blade->nggambar('admin.tag.index',$data);	
			return;
		}
	}
// --------------------------------- END BLOG TAG
// --------------------------------- START PRICELIST
	public function pricelist($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('pricelist',true);

		$data['menu']				= "pricelist";
		$data['pricelist']			= PricelistModel::orderBy('readed','desc')->desc()->get();

		PricelistModel::where('readed', 0)->update(['readed'=>'1']);

		if ($url=="detail" && $id!=null){

			$data['pricelist'] 			= PricelistModel::find($id);

			if(!isset($data['pricelist']->id)){
				redirect('superuser/pricelist');
				return;
			}

			$data['pricelist']->readed 	= 1;
			$data['pricelist']->save();

			echo $this->blade->nggambar('admin.pricelist.content',$data);	
			return;
		}
		else if ($url=="action" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['action'],['data']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$action 	= $this->input->post('action');

			switch ($action) {
				case 'delete':
				PricelistModel::whereIn('id',$this->input->post('data'))->delete();

				echo goResult(true,'Deleted!');
				break;
				case 'message':


				$rules 		= [
					'required' 	=> [
						['title']
					]
				];

				$validate 	= Validation::check($rules,'post');

				if(!$validate->auth){
					echo goResult(false,$validate->msg);
					return;
				}

				$pricelist 			= PricelistModel::whereIn('id',$this->input->post('data'))->get();

				if($pricelist){

					$mail 					= new Magicmailer;
					$email 					= $this->data;

					foreach ($pricelist as $result) {
						$mail->addAddress($result->email,$result->name);
					}

					$mail->Body    			= $this->blade->nggambar('email.admin.message',$email);	
					$mail->Subject 			= $this->input->post('title');
					$mail->AltBody 			= 'Anda Mendapatkan Pesan Dari - '.$data['config']->name;
					if($mail->send()){
						echo goResult(true,'Pesan Terkirim !');
						return;
					}
				}

				return;
				break;
				default:
				echo goResult(false,"uknown action");
				break;
			}
		}
		else if ($url=="submitfile" && $this->input->is_ajax_request()){

			$config 			= $this->data['config'];

			if (!empty($_FILES['pricelist']['name']) && $this->isDocument('pricelist')==true) {

				$filename 	= 'PRICELIST__'.seo($config->name).'__'.date('Ymdhis').'__'.getToken(12);

				$upload 	= $this->upload_materi('document','pricelist','pricelist',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($config->file_pricelist!=""){
					remFile(__DIR__.'/../../public/pricelist/'.$config->file_pricelist);
				}


				$config->file_pricelist = $upload['msg']['file_name'];
				$config->save();

				echo goResult(true,"File Price List Di Perbarui");
				return;

			}

			echo goResult(false,"File Tidak Ada / Bukan Dokumen");
			return;
			

		}
		else if ($url=="delete" && $id != null){

			$pricelist 			= PricelistModel::find($id);

			if(!isset($pricelist->id)){
				redirect('superuser/pricelist');
				return;
			}

			$pricelist->delete();
			redirect('superuser/pricelist');

		}
		else {
			echo $this->blade->nggambar('admin.pricelist.index',$data);	
			return;
		}
	}
// --------------------------------- END PRICELIST

// --------------------------------- START GALLERY
	public function gallery($url=null,$id=null){

		$data 						= $this->data;
		$data['menu']				= "gallery";
		$data['gallery']			= GalleryModel::desc()->get();

		if($url=="create"){

			$data['type']			= "create";
			echo $this->blade->nggambar('admin.gallery.content',$data);	
			return;

		}
				else if ($url=="action" && $this->input->is_ajax_request() == true) {
			$rules 		= [
						    'required' 	=> [
						        ['action'],['data']
						    ]
						  ];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$action 	= $this->input->post('action');

			switch ($action) {
				case 'delete':
					$admin 	= GalleryModel::whereIn('id',$this->input->post('data'))->get();
					GalleryModel::whereIn('id',$this->input->post('data'))->delete();
					echo goResult(true,'Deleted!');
					break;
				
				default:
					echo goResult(false,"uknown action");
					break;
			}
		}

		else if ($url == "created" && $this->input->is_ajax_request() == true){


			$rules 		= [
				'required' 	=> [
					['name'],['description'],['name_en'],['description_en']
				]
			];


			
			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$gallery 				= new GalleryModel;

			$gallery->name			= $this->input->post('name');
			$gallery->description 	= $this->input->post('description');
			$gallery->name_en			= $this->input->post('name_en');
			$gallery->description_en 	= $this->input->post('description_en');

			if(null !== $this->input->post('type')){

				if (empty($_FILES['image']['name']) || $this->isImage('image')==false) {
					echo goResult(false,"Opps! Gambar Tidak Sesuai Format Atau Tidak Ada");
					return;
				}

				$filename 	= 'GALLERY__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/gallery/','image',$filename);	

				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$gallery->image		= $upload['msg']['file_name'];	
				$gallery->type 		= 'image';
			}
			else {

				$rules 		= [
					'required' 	=> [
						['video']
					]
				];

				$validate 	= Validation::check($rules,'post');

				if(!$validate->auth){
					echo goResult(false,$validate->msg);
					return;
				}

				$gallery->video 	= $this->input->post('video');
				$gallery->type 		= "video";
			}

			if($gallery->save()){
				echo goResult(true,"Gallery Telah Di Tambahkan");
				return;
			}

		}
		else if ($url=="update" && $id!=null){

			$data['gallery'] 			= GalleryModel::find($id);
			$data['type']				= "update";

			if(!isset($data['gallery']->id)){
				redirect('superuser/gallery');
				return;
			}
			echo $this->blade->nggambar('admin.gallery.content',$data);	
			return;

		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$gallery 	= GalleryModel::find($id);

			$rules 		= [
				'required' 	=> [
					['name'],['description'],['name_en'],['description_en']
				]
			];


			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}


			if(!isset($gallery->id)){
				echo goResult(false,"Opss! Gallery Tidak Di Temukan");
				return;
			}

			if(null !== $this->input->post('type')){

				if (!empty($_FILES['image']['name'])) {

					if($this->isImage('image')==false){
						echo goResult(false,"Opps! Gambar Tidak Sesuai Format");	
						return;
					}

					$filename 	= 'GALLERY__'.seo($this->input->post('name')).'__'.date('Ymdhis');

					$upload 			= $this->upload('images/gallery/','image',$filename);	

					if($upload['auth']	== false){
						echo goResult(false,$upload['msg']);
						return;
					}

					if($gallery->type=="image"){
						if(!empty($upload['msg']['file_name'])){remFile(__DIR__.'/../../public/images/gallery/'.$gallery->image);}
					}					

					$gallery->image		= $upload['msg']['file_name'];
				}

				$gallery->type 		= "image";
				$gallery->video 	= null;
			}
			else{

				if($gallery->type=="image"){
					remFile(__DIR__.'/../../public/images/gallery/'.$gallery->image);
				}

				$rules 		= [
					'required' 	=> [
						['video']
					]
				];

				$validate 	= Validation::check($rules,'post');

				if(!$validate->auth){
					echo goResult(false,$validate->msg);
					return;
				}

				$gallery->image 	= null;
				$gallery->video 	= $this->input->post('video');
				$gallery->type 		= "video";
			}

			
			$gallery->name			= $this->input->post('name');
			$gallery->description 	= $this->input->post('description');
			$gallery->name_en		= $this->input->post('name_en');
			$gallery->description_en= $this->input->post('description_en');
			if($gallery->save()){
				echo goResult(true,"Gallery Telah Di Perbarui");
				return;
			}


		}
		else if ($url=="delete" && $id != null){

			$gallery 				= GalleryModel::find($id);

			if(!isset($gallery->id)){
				redirect('superuser/gallery');
				return;
			}

			if($gallery->type=="image"){
				if(file_exists("images/gallery/{$gallery->image}")){
					remFile(__DIR__.'/../../public/images/gallery/'.$gallery->image);
				}
			}

			
			$gallery->delete();

			redirect('superuser/gallery');
		}
		else {
			echo $this->blade->nggambar('admin.gallery.index',$data);	
			return;
		}
	}
// --------------------------------- END GALLERY

// --------------------------------- START SLIDER
	public function slider($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('slider',true);

		$data['menu']				= "slider";
		$data['slider']				= SliderModel::desc()->get();


		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.slider.content',$data);	
			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['name'],['name_en'],['url']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$slider 				= new SliderModel;

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==false) {
				echo goResult(false,"Opss! Gambar Tidak Ada Atau Tidak Sesuai");
				return;
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'SLIDER__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/slider','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$slider->image= $upload['msg']['file_name'];
			}
			

			$slider->name			= $this->input->post('name');
			$slider->name_en		= $this->input->post('name_en');
			$slider->url			= $this->input->post('url');
			$slider->status 		= (null !==$this->input->post('status') ? 0 : 1);

			if($slider->save()){
				echo goResult(true,"Slider Baru Telah Di Tambahkan");
				return;
			}
		}
		else if ($url=="update" && $id!=null){

			$data['slider'] 		= SliderModel::find($id);

			$data['type']			= "update";

			if(!isset($data['slider']->id)){
				redirect('superuser/slider');
				return;
			}

			echo $this->blade->nggambar('admin.slider.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$slider 					= SliderModel::find($id);

			if(!isset($slider->id)){
				echo goResult(false,"Opss! Slider Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['name_en'],['url']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'SLIDER__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/slider','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}

				if($slider->image!==""){
					remFile(__DIR__.'/../../public/images/slider/'.$slider->image);
				}						

				$slider->image= $upload['msg']['file_name'];
			}
			

			$slider->name			= $this->input->post('name');
			$slider->name_en		= $this->input->post('name_en');
			$slider->url			= $this->input->post('url');
			$slider->status 		= (null !==$this->input->post('status') ? 0 : 1);

			if($slider->save()){
				echo goResult(true,"Slider Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){

			$slider 			= SliderModel::find($id);

			if(!isset($slider->id)){
				redirect('superuser/slider');
				return;
			}

			if($slider->image!=""){
				remFile(__DIR__.'/../../public/images/slider/'.$slider->image);
			}
			
			$slider->delete();

			redirect('superuser/slider');
		}
		else {
			echo $this->blade->nggambar('admin.slider.index',$data);	
			return;
		}
	}
// --------------------------------- END SLIDER

// --------------------------------- START siswa
	public function siswa($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('siswa',true);

		$data['menu']				= "siswa";
		$data['siswa']				= SiswaModel::desc()->get();


		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.siswa.content',$data);	
			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['name'],['NIS']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$siswa 				= new SiswaModel;

			$siswa->name			= $this->input->post('name');
			$siswa->NIS			= $this->input->post('NIS');
			$siswa->status 		= (null !==$this->input->post('status') ? 0 : 1);

			if($siswa->save()){
				echo goResult(true,"Siswa Baru Telah Di Tambahkan");
				return;
			}
		}
		else if ($url=="update" && $id!=null){

			$data['siswa'] 		= SiswaModel::find($id);

			$data['type']			= "update";

			if(!isset($data['siswa']->id)){
				redirect('superuser/siswa');
				return;
			}

			echo $this->blade->nggambar('admin.siswa.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$siswa 					= SiswaModel::find($id);

			if(!isset($siswa->id)){
				echo goResult(false,"Opss! Siswa Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['NIS']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}


			$siswa->name			= $this->input->post('name');
			$siswa->NIS			= $this->input->post('NIS');
			$siswa->status 		= (null !==$this->input->post('status') ? 0 : 1);

			if($siswa->save()){
				echo goResult(true,"Siswa Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){

			$siswa 			= SiswaModel::find($id);

			if(!isset($siswa->id)){
				redirect('superuser/siswa');
				return;
			}

			if($siswa->image!=""){
				remFile(__DIR__.'/../../public/images/siswa/'.$siswa->image);
			}
			
			$siswa->delete();

			redirect('superuser/siswa');
		}
		else {
			echo $this->blade->nggambar('admin.siswa.index',$data);	
			return;
		}
	}
// --------------------------------- END SLIDER
// --------------------------------- START INBOX
	public function inbox($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('inbox',true);

		$data['menu']				= "inbox";
		$data['inbox']				= InboxModel::orderBy('readed','desc')->desc()->get();

		if ($url=="detail" && $id!=null){

			$data['inbox'] 			= InboxModel::find($id);

			if(!isset($data['inbox']->id)){
				redirect('superuser/inbox');
				return;
			}

			$data['inbox']->readed 	= 1;
			$data['inbox']->save();

			echo $this->blade->nggambar('admin.inbox.content',$data);	
			return;
		}
		else if ($url=="action" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['action'],['data']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$action 	= $this->input->post('action');

			switch ($action) {
				case 'delete':
				InboxModel::whereIn('id',$this->input->post('data'))->delete();
				echo goResult(true,'Deleted!');
				break;
				case 'respond':
				InboxModel::whereIn('id',$this->input->post('data'))->update(['respond'=>1,'readed'=>1]);
				echo goResult(true,"Telah Di Tandai");
				break;
				case 'message':


				$rules 		= [
					'required' 	=> [
						['title']
					]
				];

				$validate 	= Validation::check($rules,'post');

				if(!$validate->auth){
					echo goResult(false,$validate->msg);
					return;
				}

				$inbox 			= InboxModel::whereIn('id',$this->input->post('data'))->get();

				if($inbox){

					$mail 					= new Magicmailer;
					$email 					= $this->data;

					foreach ($inbox as $result) {
						$mail->addAddress($result->email,$result->name);
					}

					$mail->Body    			= $this->blade->nggambar('email.admin.message',$email);	
					$mail->Subject 			= $this->input->post('title');
					$mail->AltBody 			= 'Anda Mendapatkan Pesan Dari - '.$data['config']->name;
					if($mail->send()){
						InboxModel::whereIn('id',$this->input->post('data'))->update(['respond'=>1,'readed'=>1]);
						echo goResult(true,'Pesan Terkirim !');
						return;
					}
				}

				return;
				break;
				default:
				echo goResult(false,"uknown action");
				break;
			}
		}
		else if ($url=="delete" && $id != null){

			$inbox 			= InboxModel::find($id);

			if(!isset($inbox->id)){
				redirect('superuser/inbox');
				return;
			}

			$inbox->delete();
			redirect('superuser/inbox');

		}
		else {
			echo $this->blade->nggambar('admin.inbox.index',$data);	
			return;
		}
	}
// --------------------------------- END INBOX

// --------------------------------- START 	SOSMED
	public function sosmed($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('sosmed',true);

		$data['menu']				= "sosmed";
		$data['sosmed']				= SosmedModel::desc()->get();

		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.sosmed.content',$data);	
			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['name'],['type'],['url']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$sosmed 				= new SosmedModel;

			$sosmed->name 			= $this->input->post('name');
			$sosmed->type 			= $this->input->post('type');
			$sosmed->url 			= $this->input->post('url');

			if($sosmed->save()){
				echo goResult(true,"Sukses! , Sosial Media Telah Di Tambahkan");
				return;
			}
		}
		else if ($url=="update" && $id!=null){

			$data['sosmed'] 		= SosmedModel::find($id);
			$data['type']			= "update";

			if(!isset($data['sosmed']->id)){
				redirect('superuser/sosmed');
				return;
			}

			echo $this->blade->nggambar('admin.sosmed.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$sosmed 				= SosmedModel::find($id);

			if(!isset($sosmed->id)){
				echo goResult(false,"Opps! Sosial Media Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['type'],['url']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$sosmed->name 			= $this->input->post('name');
			$sosmed->type 			= $this->input->post('type');
			$sosmed->url 			= $this->input->post('url');

			if($sosmed->save()){
				echo goResult(true,"Sosial Media Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){

			$sosmed 				= SosmedModel::find($id);

			if(!isset($sosmed->id)){
				redirect('superuser/sosmed');
				return;
			}
			
			$sosmed->delete();
			redirect('superuser/sosmed');

		}
		else {
			echo $this->blade->nggambar('admin.sosmed.index',$data);	
			return;
		}
	}
// --------------------------------- END SOSMED

// --------------------------------- START SEO
	public function seo($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('seo',true);

		$data['menu']				= "seo";
		$data['seo']				= SeoModel::find(1);

		if ($url == "save" && $this->input->is_ajax_request() == true){

			$seo 					= SeoModel::find(1);

			if(!$seo){
				echo goResult(false,"Seo Content Not Found!");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['description'],['keyword'],['author'],['title']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'SEO__IMAGE__'.date('Ymdhis');

				$upload 	= $this->upload('images/website','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($seo->image!=""){
					if(!empty($upload['msg']['file_name'])){remFile(__DIR__.'/../../public/images/website/'.$seo->image);}
				}
				
				$seo->image= $upload['msg']['file_name'];
				
			}

			$seo->title 		= $this->input->post('title');
			$seo->author 		= $this->input->post('author');
			$seo->keyword 		= $this->input->post('keyword');
			$seo->description 	= $this->input->post('description');
			$seo->fbpixel 		= $this->input->post('fbpixel');
			$seo->analytic 		= $this->input->post('analytic');

			if($seo->save()){
				echo goResult(true,"Pengaturan SEO di Perbarui");
				return;
			}
		}
		else {
			echo $this->blade->nggambar('admin.seo.index',$data);	
			return;
		}
	}
// --------------------------------- END SEO
// --------------------------------- START
	public function menupengunjung($url=null,$id=null){
		$data 						= $this->data;
		$data['menu']				= "menupengunjung";
		$data['menupengunjung']			= MenupengunjungModel::desc()->get();


		if($url=="create"){
			$data['type']			= "create";
			echo $this->blade->nggambar('admin.menupengunjung.content',$data);
			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['judul']
				]
			];

			$uploaded 	= false;

			if ($this->validation($rules,'post')==false){
				echo goResult(false,"Opss! Form Tidak Benar");
				return;
			}

			$menupengunjung 			= new MenupengunjungModel;

			$menupengunjung->judul		= $this->input->post('judul');
			$menupengunjung->link		= $this->input->post('link');
			$menupengunjung->urutan		= $this->input->post('urutan');
			$menupengunjung->aktif 		= (null !==$this->input->post('aktif') ? '0' : '1' );

			if($menupengunjung->save()){
				echo goResult(true,"Sukses! , Data Baru Telah Di Tambahkan");
				return;
			}
		}
		else if ($url=="update" && $id!=null){

			$data['menupengunjung'] 	= MenupengunjungModel::find($id);
			$data['type']				= "update";

			if(!isset($data['menupengunjung']->id)){
				redirect('superuser/menupengunjung');
				return;
			}
			echo $this->blade->nggambar('admin.menupengunjung.content',$data);
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$menupengunjung 				= MenupengunjungModel::findorfail($id);

			if(!isset($menupengunjung->id)){
				echo goResult(false,"Opss! Data Tidak Di Temukan");
				return;
			}

			$rules = [
				'required' 	=> [
					['judul']
				]
			];


			if($this->validation($rules,'post')==false){
				echo goResult(false,"Opss! Form Belum Benar");
				return;
			}

			$menupengunjung->judul		= $this->input->post('judul');
			$menupengunjung->link		= $this->input->post('link');
			$menupengunjung->urutan 	= $this->input->post('urutan');
			$menupengunjung->aktif 		= (null !==$this->input->post('aktif') ? '0' : '1');

			if($menupengunjung->save()){
				echo goResult(true,"Data Telah Di Perbarui");
				return;
			}
		}
		else if ($url=="deleted" && $id != null){

			$menupengunjung 			= MenupengunjungModel::find($id);
			if(!isset($menupengunjung->id)){
				redirect('superuser/menupengunjung');
				return;
			}

			$menupengunjung->delete();

			redirect('superuser/menupengunjung');
		}
		else {
			echo $this->blade->nggambar('admin.menupengunjung.index',$data);
			return;
		}
	}
// --------------------------------- END
// --------------------------------- START AUTH
	public function auth($url=null,$id=null){
		$data 						= $this->data;

		$this->middleware->access('auth',true);

		$data['menu']				= "auth";
		$data['auth']				= AuthModel::desc()->get();

		if($url=="create"){
			$data['type']			= "create";
			$data['authrule'] 		= [];
			echo $this->blade->nggambar('admin.auth.content',$data);	
			return;
		}
		else if($url=="checkusername" && $this->input->is_ajax_request()){
			$rules 		= [
				'required' 	=> [
					['username']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['username',5]
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$check 				= AuthModel::where('username',$this->input->post('username'))->first();

			if($check){

				if($this->input->post('id')==$check->id){
					echo goResult(true,"username bisa di pakai");
					return;
				}

				echo goResult(false,"Opps! Username Telah Terpakai");
				return;
			}

			echo goResult(true,"username bisa di pakai");

			return;
		}
		else if($url=="changestatus" && $this->input->is_ajax_request()){
			$rules 		= [
				'required' 	=> [
					['status'],['id']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$auth 				= AuthModel::find($this->input->post('id'));
			if(!$auth){
				echo goResult(false,"uknown id");
				return;
			}
			$auth->status 		= $this->input->post('status');
			$auth->save();
			echo goResult(true,"changed");

			return;
		}
		else if ($url == "created" && $this->input->is_ajax_request() == true){

			$rules 		= [
				'required' 	=> [
					['name'],['username'],['password'],['conf_password']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['password',8],['conf_password',8],['username',5]
				]
			];
			if(!$this->input->post('access_all')){
				$rules['required'][] 	= ['access'];
			}

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			if($this->input->post('password')!==$this->input->post('conf_password')){
				echo goResult(false,"Password dan konfirmasi password tidak cocok");
				return;
			}

			$check 				= AuthModel::where('username',$this->input->post('username'))->first();

			if($check){
				echo goResult(false,"Opps! Username Telah Terpakai");
				return;
			}

			$auth 				= new AuthModel;

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'ADMIN__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/admin','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				$auth->image= $upload['msg']['file_name'];
			}
			

			$auth->name				= $this->input->post('name');
			$auth->username			= $this->input->post('username');
			$auth->password 		= DefuseLib::encrypt($this->input->post('password'));
			$auth->ipaddress 		= $this->input->ip_address();
			$auth->lastlog 			= date('Y-m-d H:i:s ');
			$auth->status 			= (null !==$this->input->post('status') ? 'active' : 'blocked');
			if($auth->save()){

				if($this->input->post('access_all')){
					$access 				= new AuthRuleModel;
					$access->id_superuser	= $auth->id;
					$access->menu 			= 'all';
					$access->save();
				}
				else {
					foreach ($this->input->post('access') as $result) {
						$access 				= new AuthRuleModel;
						$access->id_superuser	= $auth->id;
						$access->menu 			= $result;
						$access->save();
					}	
				}

				echo goResult(true,"Akun Administrator Di Tambahkan");
				return;
			}
		}
		else if ($url=="update" && $id!=null){

			$data['auth'] 			= AuthModel::find($id);

			$data['authrule'] 		= [];

			foreach ($data['auth']->rule as $result) {
				array_push($data['authrule'], $result->menu);
			}

			$data['type']			= "update";

			if(!isset($data['auth']->id)){
				redirect('superuser/auth');
				return;
			}

			echo $this->blade->nggambar('admin.auth.content',$data);	
			return;
		}
		else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

			$auth 		= AuthModel::find($id);

			if(!$auth){
				echo goResult(false,"Akun Administrator Tidak Di Temukan");
				return;
			}

			$rules 		= [
				'required' 	=> [
					['name'],['username'],['password'],['conf_password']
				],
				'alphaNum'	=> [
					['username']
				],
				'lengthMin'	=>	[
					['password',8],['conf_password',8],['username',5]
				]
			];

			if(!$this->input->post('access_all')){
				$rules['required'][] 	= ['access'];
			}

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}


			if($this->input->post('password')!==$this->input->post('conf_password')){
				echo goResult(false,"Password dan konfirmasi password tidak cocok");
				return;
			}

			$check 				= AuthModel::where('username',$this->input->post('username'))->first();

			if($check){
				if($check->id!==$auth->id){
					echo goResult(false,"Opps! Username Telah Terpakai");
					return;	
				}
			}

			if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

				$filename 	= 'ADMIN__'.seo($this->input->post('name')).'__'.date('Ymdhis');

				$upload 	= $this->upload('images/admin','image',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($auth->image!==""){
					remFile(__DIR__.'/../../public/images/admin/'.$auth->image);
				}

				$auth->image= $upload['msg']['file_name'];
			}
			

			$auth->name				= $this->input->post('name');
			$auth->username			= $this->input->post('username');
			$auth->password 		= DefuseLib::encrypt($this->input->post('password'));
			$auth->ipaddress 		= $this->input->ip_address();
			$auth->lastlog 			= date('Y-m-d H:i:s ');
			$auth->status 			= (null !==$this->input->post('status') ? 'active' : 'blocked');
			if($auth->save()){

				AuthRuleModel::where('id_superuser',$auth->id)->delete();

				if($this->input->post('access_all')){
					$access 				= new AuthRuleModel;
					$access->id_superuser	= $auth->id;
					$access->menu 			= 'all';
					$access->save();
				}
				else {
					foreach ($this->input->post('access') as $result) {
						$access 				= new AuthRuleModel;
						$access->id_superuser	= $auth->id;
						$access->menu 			= $result;
						$access->save();
					}	
				}

				echo goResult(true,"Akun Administrator Di Perbarui");
				return;
			}
		}
		else if ($url=="delete" && $id != null){


			$auth 			= AuthModel::find($id);

			if(!$auth){
				redirect('superuser/auth');
				return;
			}

			AuthRuleModel::where('id_superuser',$auth->id)->delete();

			if($auth->image!=""){
				remFile(__DIR__.'/../../public/images/admin/'.$auth->image);
			}
			
			$auth->delete();

			redirect('superuser/auth');
		}
		else {
			echo $this->blade->nggambar('admin.auth.index',$data);	
			return;
		}
	}
// --------------------------------- END AUTH

// --------------------------------- MAIN SUbSCRIBE
	public function subscribe($page=null,$id=null){

		$data 				= $this->data;
		$data['menu'] 		= 'subscribe';

		$this->middleware->access('subscribe',true);

		if($page == 'delete' && $id != null){

			$subscribe 			= subscribeModel::find($id);

			if(!isset($subscribe->id)){
				redirect('superuser/subscribe');
				return;
			}
			echo "tes";
			$subscribe->delete();

			redirect('superuser/subscribe');

		}elseif($page == 'respon' && $id != null){

			$subscribe 			= subscribeModel::find($id);

			if(!isset($subscribe->id)){
				redirect('superuser/subscribe');
				return;
			}
			
			$subscribe->status 		= '1';
			$subscribe->save();

			redirect('superuser/subscribe');

		}else{

			$data['subscribe'] 	= subscribeModel::desc()->get();

			echo $this->blade->nggambar('admin.subscribe.index', $data);

		}
	}
// --------------------------------- END MAIN SUbSCRIBE

// --------------------------------- START CONFIGURATION 
	public function config($page='about'){
		$data 						= $this->data;
		$data['menu']				= 'config';
		$this->middleware->access('config',true);
		switch ($page) {
			case 'about':
			$data['submenu']	= 'about';
			echo $this->blade->nggambar('admin.config.about',$data);
			break;
			case 'aboutsave':
			$config 	= $data['config'];

			$rules 		= [
				'required' 	=> [
					['name'],['address'],['address_en'],['gmap'],['gmap_query'],['description'],['description_en']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$config->name 			= $this->input->post('name');
			$config->description 	= $this->input->post('description');
			$config->description_en = $this->input->post('description_en');
			$config->address_en 		= $this->input->post('address_en');
			$config->gmap 			= $this->input->post('gmap');
			$config->gmap_query 	= $this->input->post('gmap_query');
			$config->save();
			echo goResult(true,"Pengaturan , Tentang Website Di Simpan");
			return;

			break;
			case 'contact':
			$data['submenu']	= 'contact';
			echo $this->blade->nggambar('admin.config.contact',$data);
			break;
			case 'contactsave':
			$config 	= $data['config'];

			$rules 		= [
				'required' 	=> [
					['email'],['phone'],['whatsapp'],['bbm'],['contact']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$config->email 		= $this->input->post('email');
			$config->phone	 	= $this->input->post('phone');
			$config->whatsapp 	= $this->input->post('whatsapp');
			$config->bbm 		= $this->input->post('bbm');
			$config->contact 	= $this->input->post('contact');

			$config->save();
			echo goResult(true,"Pengaturan , Kontak Di Simpan");
			return;
			break;
			case 'appearance':
			$data['submenu']	= 'appearance';
			echo $this->blade->nggambar('admin.config.appearance',$data);
			break;
			case 'appearancesave':
			$config 			= $data['config'];

				// Icon
			if (!empty($_FILES['icon']['name']) && $this->isImage('icon')==true) {
				$filename 	= 'ICON__'.date('Ymdhis');
				$upload 	= $this->upload('images/website','icon',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}	

				if($config->icon!==""){
					remFile(__DIR__.'/../../public/images/website/'.$config->icon);
				}

				$config->icon= $upload['msg']['file_name'];
			}

			if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {
				$filename 	= 'LOGO__'.date('Ymdhis');
				$upload 	= $this->upload('images/website','logo',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}

				if($config->logo!==""){
					remFile(__DIR__.'/../../public/images/website/'.$config->logo);
				}

				$config->logo= $upload['msg']['file_name'];
			}

			if (!empty($_FILES['logo_white']['name']) && $this->isImage('logo_white')==true) {
				$filename 	= 'LOGO_WHITE__'.date('Ymdhis');
				$upload 	= $this->upload('images/website','logo_white',$filename);
				if($upload['auth']	== false){
					echo goResult(false,$upload['msg']);
					return;
				}

				if($config->logo_white!==""){
					remFile(__DIR__.'/../../public/images/website/'.$config->logo_white);
				}

				$config->logo_white = $upload['msg']['file_name'];
			}

			$config->save();

			echo goResult(true,'Tampilan Di Simpan');
			return;

			break;
			case 'close':
			$data['submenu']	= 'close';
			echo $this->blade->nggambar('admin.config.close',$data);
			break;
			case 'closesave':

			$config 	= $data['config'];

			$rules 		= [
				'required' 	=> [
					['description']
				]
			];

			$validate 	= Validation::check($rules,'post');

			if(!$validate->auth){
				echo goResult(false,$validate->msg);
				return;
			}

			$config->close_message 	= $this->input->post('description');
			$config->status 		= (null !== $this->input->post('status')) ? 0 : 1;

			$config->save();
			echo goResult(true,"Pengaturan , Maintenance Di Simpan");
			break;
			default:
			redirect('superuser');
			break;
		}
	}
// --------------------------------- END CONFIGURATION

// PRIVATE SECTION ---------------------------------------------
	private function validation($rules,$type){
		if($type=="post"){
			$v = new Valitron\Validator($_POST);	
		}
		else {
			$v = new Valitron\Validator($_GET);	
		}

		
		$v->rules($rules);
		if($v->validate()){
			return true;
		}
		else {
			return false;
		}
	}

	private function upload_files($path,$files,$filename=false)
	{
		$config = array(
			'upload_path'   => $path,
			'allowed_types' => 'jpg|gif|png|jpeg',
			'max_size'		=> '2000',
			'overwrite'     => false,
		);

		if($filename){
			$config['file_name'] 	= $filename;
		}else {
			$config['encrypt_name'] 	= FALSE;
		}

		$this->load->library('upload', $config);

		$images 		= array();
		$data['msg']	= array();
		$data['auth']	= false;
		foreach ($files['name'] as $key => $image) {
			$_FILES['image[]']['name']		= $files['name'][$key];
			$_FILES['image[]']['type']		= $files['type'][$key];
			$_FILES['image[]']['tmp_name']	= $files['tmp_name'][$key];
			$_FILES['image[]']['error']		= $files['error'][$key];
			$_FILES['image[]']['size']		= $files['size'][$key];

			$this->upload->initialize($config);

			if ($this->upload->do_upload('image[]')) {
				$data['auth']		= true;
				array_push($data['msg'],$this->upload->data());
			} else {
				$data['auth']		= ($data['auth']==true) ? true : false;
				array_push($data['msg'],$this->upload->display_errors());
			}
		}

		return $data;
	}


	private function upload($dir,$name ='userfile',$filename=false){
		$config['upload_path']      = $dir;
		$config['allowed_types']    = 'gif|jpg|png|jpeg';
		$config['max_size']         = 2000;

		if($filename){
			$config['file_name'] 	= $filename;
		}else {
			$config['encrypt_name'] 	= FALSE;
		}

		$this->load->library('upload', $config);

		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload($name))
		{		
			$data['auth'] 	= false;
			$data['msg'] 	= $this->upload->display_errors();
			return $data;
		}
		else
		{
			$data['auth']	= true;
			$data['msg']	= $this->upload->data();
			return $data;
		}
	}

	private function upload_materi($type='file',$dir,$name ='userfile',$filename=null){
		$config['upload_path']      = $dir;
		$config['allowed_types'] 	= "*";

		if($type=='document'){
			$config['allowed_types']    = 'pdf|doc|docx|ppt|pptx|xls|xlsx|txt|text|jpeg|jpg|png';
		}
		else if ($type=='video'){
			$config['allowed_types']    = 'mkv|mp4|avi|flv|mov';
		}
		else if($type=="audio"){
			//$config['allowed_types']    = 'm4a|mp3|M4A|ogg|wav|mp4|x-m4a';	
			
		}
		else {
			$config['allowed_types']    = 'iso|7z|rar|zip';
		}


		if($filename==null){
			$config['encrypt_name'] 	= FALSE;
		}
		else {
			$config['file_name'] 		= $filename;
		}

		$this->load->library('upload');
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($name))
		{		
			$data['auth'] 	= false;
			$data['msg'] 	=$this->upload->display_errors();
			return $data;
		}
		else
		{
			$data['auth']	= true;
			$data['msg']	= $this->upload->data();
			return $data;
		}
	}

	private function uploadVideo($dir,$name,$filename=null){
		$config['upload_path']      = $dir;
		$config['allowed_types']    = 'mkv|mp4|avi|flv|mov';

		if($filename==null){
			$config['encrypt_name'] 	= FALSE;
		}
		else {
			$config['file_name'] 		= $filename;
		}

		$this->load->library('upload');
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($name))
		{		
			$data['auth'] 	= false;
			$data['msg'] 	=$this->upload->display_errors();
			return $data;
		}
		else
		{
			$data['auth']	= true;
			$data['msg']	= $this->upload->data();
			return $data;
		}
	}

	private function isVideo($file){
		$mime 		= ['video/mp4','video/x-matroska','video/x-msvideo','video/quicktime','video/x-flv'];
		if ( in_array($_FILES[$file]['type'], $mime)){
			return true;
		}
		else {
			return false;
		}
	}

	private function isAudio($file){
		$mime 		= ['audio/mpeg','audio/mp3','audio/x-m4a','audio/mp4','audio/x-aiff','audio/ogg','audio/vnd.wav'];
		if ( in_array($_FILES[$file]['type'], $mime)){
			return true;
		}
		else {
			return false;
		}
	}

	private function isImage($file){
		if ((($_FILES[$file]['type'] == 'image/gif') || ($_FILES[$file]['type'] == 'image/jpeg') || ($_FILES[$file]['type'] == 'image/png'))){
			return true;
		}
		else {
			return false;
		}
	}

	private function isDocument($file){
		if ( $_FILES[$file]['type'] == 'application/pdf' || $_FILES[$file]['type'] == 'application/msword' 
			|| $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' 
			|| $_FILES[$file]['type'] == 'application/vnd.ms-excel'  
			|| $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' 
			|| $_FILES[$file]['type'] == 'application/vnd.ms-powerpoint' 
			|| $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.presentationml.presentation' 
			|| $_FILES[$file]['type'] == 'text/plain'  
			|| $_FILES[$file]['type'] == 'image/gif'
			|| $_FILES[$file]['type'] == 'image/jpeg' 
			|| $_FILES[$file]['type'] == 'image/png' 
		){

			return true;
	}
	else {
		return false;
	}
}

private function isArchive($file){
	if ( $_FILES[$file]['type'] == 'application/x-compressed' 
		|| $_FILES[$file]['type'] == 'application/x-zip-compressed' 
		|| $_FILES[$file]['type'] == 'application/zip' 
		|| $_FILES[$file]['type'] == 'multipart/x-zip' 
		|| $_FILES[$file]['type'] == 'application/x-rar-compressed' 
		|| $_FILES[$file]['type'] == 'application/octet-stream' 
		|| $_FILES[$file]['type'] == 'application/x-7z-compressed'  || $_FILES[$file]['type'] == 'application/x-gtar'   ){
		return true;
}
else {
	return false;
}
}
// END PRIVATE SECTION
}