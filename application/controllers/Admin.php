<?php

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

         if(!isset($_SESSION['site_lang'])){
            $_SESSION['site_lang']= 'english';
        }
        $siteLang =$this->session->userdata('site_lang');
        if($siteLang){
            $this->lang->load('information',$siteLang);
        } else {
            $this->lang->load('information','english');
        }
        $this->data['lang']                 =  $siteLang;
        $this->data['language']             = $this->lang->language;
        $this->data['config'] = ConfigModel::find(1);
        $this->data['seo'] = SeoModel::find(1);
        $this->data['sosmed'] = SosmedModel::desc()->get();
        $this->data['news'] = BlogModel::notDraft()->desc()->take(3)->get();
        $this->data['popular'] = BlogModel::notDraft()->orderBy('view', 'desc')->take(3)->get();
        $this->data['news'] = BlogModel::notDraft()->desc()->take(3)->get();
        $this->data['menupengunjung'] = MenupengunjungModel::Aktif()->orderby('urutan', 'asc')->get();
        $this->data['menu'] = "admin";
        $this->data['name'] = $this->session->userdata('admin_name');
        $this->data['authmain']             = AdminModel::find($this->session->userdata('id'));
        $this->data['terbaru'] = BlogModel::notDraft()->desc()->take(4)->get();
                $this->data['notif_inbox']          = InboxModel::notReaded()->desc()->get();
        $this->data['notif_pricelist']      = PricelistModel::notReaded()->desc()->get();

        $this->data['notif_total']          = count($this->data['notif_inbox']) + count($this->data['notif_pricelist']);
        //statistik pengunjung
        $ip = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
        $tanggal = date("Ymd"); // Mendapatkan tanggal sekarang
        $waktu = time();
        $date_min = strtotime("-1 day");
        $kemarin = date('Y-m-d', $date_min);
        $tahun_ini = date('Y');
        $bulan_ini = date('m');
        $bataswaktu = time() - 300;

        // Mencek berdasarkan IPnya, apakah user sudah pernah mengakses hari ini
        $s = StatistikModel::where('ip', $ip)->where('tanggal', $tanggal)->get();

        if (count($s) == 0) {
            $statistik = new StatistikModel;
            $statistik->ip = $ip;
            $statistik->tanggal = $tanggal;
            $statistik->hits = 1;
            $statistik->online = $waktu;
            $statistik->save();
        } else {
            $statis = StatistikModel::where('ip', $ip)->get()->first();
            $statis->ip = $ip;
            $statis->tanggal = $tanggal;
            $statis->hits = $statis->hits + 1;
            $statis->online = $waktu;
            $statis->save();
        }

        $this->data['pengunjung'] = StatistikModel::where('tanggal', $tanggal)->groupBy('ip')->count('hits');
        $this->data['kemarin'] = StatistikModel::where('tanggal', $kemarin)->get()->count('hits');
        $this->data['perbulan'] = StatistikModel::where('tanggal', 'LIKE', '%' . $tahun_ini . '-' . $bulan_ini . '%')->get()->count('hits');
        $this->data['pertahun'] = StatistikModel::where('tanggal', 'LIKE', '%' . $tahun_ini . '%')->get()->count('hits');
        $this->data['total'] = StatistikModel::get()->count('hits');
        $this->data['online'] = StatistikModel::where('online', '>', $bataswaktu)->get()->count('hits');
        $this->data['category'] = CategoryBlogModel::desc()->get();

        $this->blade->sebarno('ctrl', $this);
    }
    public function switchLang($language = ""){
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        return redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function index($page = "index", $id = null)
    {
        $data = $this->data;
        if (!$this->session->userdata('admin_auth')) {
            redirect('admin/masuk');
        }
        $data['menu'] = "admin";
        $data['profile'] = AdminModel::find($this->session->userdata('admin_id'));
        $data['rank'] = AdminModel::get();
        $data['submenu'] = 'dashboard';
        echo $this->blade->nggambar('website.admin.index', $data);
    }

    public function masuk($url = 'index')
    {
        $data = $this->data;
        $data['menu'] = 'admin';
        if ($this->session->userdata('admin_auth')) {
            redirect('admin');
        }
        if ($url == 'auth' && $this->input->is_ajax_request() == true) {
            if ($this->session->userdata('admin_auth')) {
                echo goResult(false, "Anda Telah Masuk Admin.");
                return;
            }

            if (strpos($this->input->post('username'), '@') > 1) {
                $where = array(
                    'email' => $this->input->post('username'),
                );
            } else {
                $where = array(
                    'username' => $this->input->post('username'),
                );
            }
            $auth = AdminModel::where($where)->first();

            if (!isset($auth->id)) {
                echo goResult(false, "Opps! Username Tidak Di Temukan");
                return;
            }

            if (DefuseLib::decrypt($auth->password) !== $this->input->post('password')) {
                echo goResult(false, "Opps! Maaf Password Anda Tidak Cocok");
                return;
            }

            if ($auth->status == "blocked") {
                echo goResult(false, "Opps! Maaf Akun Anda Di Blokir");
                return;
            }

            $auth->lastlog = date('Y-m-d H:i:s ');
            $auth->ipaddress = $this->input->ip_address();
            $auth->save();

            $newdata = array(
                'admin_auth' => TRUE,
                'admin_id' => $auth->id,
                'admin_name' => $auth->name
            );

            $this->session->set_userdata($newdata);

            echo goResult(true, "Selamat datang...");
            return;
        } else {
            echo $this->blade->nggambar('website.admin.login', $data);
        }
    }
    // --------------------------------- START Members
    public function member($url=null,$id=null){
        $data                       = $this->data;

        $this->middleware->access('member',true);

        $data['menu']               = "member";
        $data['member']             = MemberModel::desc()->get();

        if($url=="create"){
            $data['type']           = "create";
            echo $this->blade->nggambar('admin.member.content',$data);  
            return;
        }
        else if($url=="checkusername" && $this->input->is_ajax_request()){
            $rules      = [
                'required'  => [
                    ['username']
                ],
                'alphaNum'  => [
                    ['username']
                ],
                'lengthMin' =>  [
                    ['username',5]
                ]
            ];

            $validate   = Validation::check($rules,'post');

            if(!$validate->auth){
                echo goResult(false,$validate->msg);
                return;
            }

            $check              = MemberModel::where('username',$this->input->post('username'))->first();

            if($check){

                if($this->input->post('id')==$check->id){
                    echo goResult(true,"username bisa di pakai");
                    return;
                }

                echo goResult(false,"Opps! Username Telah Terpakai");
                return;
            }

            echo goResult(true,"username bisa di pakai");

            return;
        }
        else if($url=="changestatus" && $this->input->is_ajax_request()){
            $rules      = [
                'required'  => [
                    ['status'],['id']
                ]
            ];

            $validate   = Validation::check($rules,'post');

            if(!$validate->auth){
                echo goResult(false,$validate->msg);
                return;
            }

            $member             = MemberModel::find($this->input->post('id'));
            if(!$member){
                echo goResult(false,"uknown id");
                return;
            }
            $member->status         = $this->input->post('status');
            $member->save();
            echo goResult(true,"changed");

            return;
        }
        else if ($url == "created" && $this->input->is_ajax_request() == true){

            $rules      = [
                'required'  => [
                        ['name'],['username'],['password'],['conf_password']
                ],
                'alphaNum'  => [
                    ['username']
                ],
                'lengthMin' =>  [
                    ['password',8],['conf_password',8],['username',5]
                ]
            ];

            $validate   = Validation::check($rules,'post');

            if(!$validate->auth){
                echo goResult(false,$validate->msg);
                return;
            }

            if($this->input->post('password')!==$this->input->post('conf_password')){
                echo goResult(false,"Password dan konfirmasi password tidak cocok");
                return;
            }

            $check              = MemberModel::where('username',$this->input->post('username'))->first();

            if($check){
                echo goResult(false,"Opps! Username Telah Terpakai");
                return;
            }

            $member             = new MemberModel;
    if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {

                $filename   = 'LOGO__'.seo($this->input->post('name')).'__'.date('Ymdhis');

                $upload     = $this->upload('images/member','logo',$filename);
                if($upload['auth']  == false){
                    echo goResult(false,$upload['msg']);
                    return;
                }   

                $member->logo= $upload['msg']['file_name'];
            }
            if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

                $filename   = 'MEMBER__'.seo($this->input->post('name')).'__'.date('Ymdhis');

                $upload     = $this->upload('images/member','image',$filename);
                if($upload['auth']  == false){
                    echo goResult(false,$upload['msg']);
                    return;
                }   

                $member->image= $upload['msg']['file_name'];
            }
            

            $member->name               = $this->input->post('name');
            $member->username           = $this->input->post('username');
            $member->password       = DefuseLib::encrypt($this->input->post('password'));
            $member->ipaddress      = $this->input->ip_address();
            $member->lastlog            = date('Y-m-d H:i:s ');
            $member->status             = (null !==$this->input->post('status') ? 'active' : 'blocked');
                if($member->save()){
                echo goResult(true,"Akun Members Di Tambahkan");
                return;
            }
        }
        
        else if ($url=="update" && $id!=null){

            $data['member']             = MemberModel::find($id);
            $data['type']           = "update";

            if(!isset($data['admin']->id)){
                redirect('superuser/admin');
                return;
            }

            echo $this->blade->nggambar('admin.admin.content',$data);   
            return;
        }
        else if ($url=="updated" && $id!=null && $this->input->is_ajax_request() == true){

            $member         = MemberModel::find($id);

            if(!$member){
                echo goResult(false,"Akun Admin Tidak Di Temukan");
                return;
            }

            $rules      = [
                'required'  => [
                    ['name'],['username'],['password'],['conf_password']
                ],
                'alphaNum'  => [
                    ['username']
                ],
                'lengthMin' =>  [
                    ['password',8],['conf_password',8],['username',5]
                ]
            ];

            $validate   = Validation::check($rules,'post');

            if(!$validate->auth){
                echo goResult(false,$validate->msg);
                return;
            }


            if($this->input->post('password')!==$this->input->post('conf_password')){
                echo goResult(false,"Password dan konfirmasi password tidak cocok");
                return;
            }

            $check              = MemberModel::where('username',$this->input->post('username'))->first();

            if($check){
                if($check->id!==$MemberModel->id){
                    echo goResult(false,"Opps! Username Telah Terpakai");
                    return; 
                }
            }
                if (!empty($_FILES['logo']['name']) && $this->isImage('logo')==true) {

                $filename   = 'LOGO__'.seo($this->input->post('name')).'__'.date('Ymdhis');

                $upload     = $this->upload('images/member','logo',$filename);
                if($upload['auth']  == false){
                    echo goResult(false,$upload['msg']);
                    return;
                }   

                if($MemberModel->logo!==""){
                    remFile(__DIR__.'/../../public/images/member/'.$MemberModel->logo);
                }

                $MemberModel->logo= $upload['msg']['file_name'];
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image')==true) {

                $filename   = 'MEMBER__'.seo($this->input->post('name')).'__'.date('Ymdhis');

                $upload     = $this->upload('images/member','image',$filename);
                if($upload['auth']  == false){
                    echo goResult(false,$upload['msg']);
                    return;
                }   

                if($member->image!==""){
                    remFile(__DIR__.'/../../public/images/member/'.$member->image);
                }

                $member->image= $upload['msg']['file_name'];
            }
            

            $member->name               = $this->input->post('name');
            $member->username           = $this->input->post('username');
            $member->password       = DefuseLib::encrypt($this->input->post('password'));
            $member->ipaddress      = $this->input->ip_address();
            $member->lastlog            = date('Y-m-d H:i:s ');
            $member->status             = (null !==$this->input->post('status') ? 'active' : 'blocked');
            if($member->save()){
                echo goResult(true,"Akun Members Di Perbarui");
                return;
            }
        }
        else if ($url=="delete" && $id != null){


            $auth           = AuthModel::find($id);

            if(!$auth){
                redirect('admin/auth');
                return;
            }

            AuthRuleModel::where('id_superuser',$auth->id)->delete();

            if($auth->image!=""){
                remFile(__DIR__.'/../../public/images/member/'.$auth->image);
            }
            
            $auth->delete();

            redirect('superuser/member');
        }
        else {
            echo $this->blade->nggambar('admin.member.index',$data);    
            return;
        }
    }
// --------------------------------- END Members


    public function daftar($url = 'index')
    {
        $data = $this->data;
        $data['menu'] = "daftar";

        if ($url == 'confirm' && $this->input->is_ajax_request() == true) {
            $rules = [
                'required' => [
                    ['name'],
                    ['email'],
                    ['username'],
                    ['password']
                ],
                'lengthMin' => [
                    ['username', 5],
                    ['password', 8]
                ]
            ];

            $validate = Validation::check($rules, 'post');
            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            $cekmail = AdminModel::where('email', $this->input->post('email'))->first();
            $cekuser = AdminModel::where('username', $this->input->post('username'))->first();
            if (isset($cekmail->id)) {
                echo goResult(false, "Opps! Email telah digunakan.");
                return;
            } else if (isset($cekuser->id)) {
                echo goResult(false, "Opps! Username telah digunakan.");
                return;
            } else if ($this->input->post('password') !== $this->input->post('repassword')) {
                echo goResult(false, "Opps! Confirmation password not match.");
                return;
            }

            $daftar = new AdminModel;

            $daftar->name = $this->input->post('name');
            $daftar->username = $this->input->post('username');
            $daftar->email = $this->input->post('email');
            $daftar->password = DefuseLib::encrypt($this->input->post('password'));
            $daftar->status = 'pending';
            $daftar->ipaddress = $this->input->ip_address();

            $daftar->save();
            echo goResult(true, "Ok! Pendaftaran berhasil.");
        } else {
            echo $this->blade->nggambar('website.admin.register', $data);
        }
    }

    public function lostpassword($url = "index")
    {
        $data = $this->data;
        if ($this->session->userdata('admin_auth')) {
            redirect('admin');
        }
        if ($url == "submit") {
            $where = array(
                'email' => $this->input->post('email'),
            );
            $admin = AdminModel::where($where)->first();
            if (!isset($admin->id)) {
                echo goResult(false, "Email tidak ditemukan.");
            } else {
                echo goResult(true, "Silakan cek email untuk mengembalikan password.");
            }
        } else {
            echo $this->blade->nggambar('website.admin.login', $data);
            return;
        }
    }

    public function profile($url = 'index')
    {
        $data = $this->data;
        $data['menu'] = "profile";
        $data['profile'] = AdminModel::find($this->session->userdata('admin_id'));

        $admin = AdminModel::find($this->session->userdata('admin_id'));

        if ($url == 'confirm') {
            $rules = [
                'required' => [
                    ['name'],
                    ['image']
                ]
            ];

            $validate = Validation::check($rules, 'post');
            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == false) {
                echo goResult(false, "Gambar Tidak ada");
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == true) {

                $filename = 'PROFILE__' . seo($this->input->post('name')) . '__' . date('Ymdhis');

                $upload = $this->upload('images/profile/', 'image', $filename);

                if ($upload['auth'] == false) {
                    echo goResult(false, $upload['msg']);
                    return;
                }

                if (!empty($upload['msg']['file_name'])) {
                    remFile(__DIR__ . '/../../public/images/profile/' . $admin->image);
                }

                $admin->image = $upload['msg']['file_name'];
            }

            $admin->name = $this->input->post('name');
//            $member->image = $this->input->post('bla');

            $admin->save();
            echo goResult(true, "Ok! Profile telah terupdate.");
        } else {
            echo $this->blade->nggambar('website.admin.profile', $data);
        }
    }

    public function updatePassword($url = 'index')
    {
        $data = $this->data;
        $data['profile'] = AdminModel::find($this->session->userdata('admin_id'));
        $data['menu'] = "ubah";

        $admin = AdminModel::find($this->session->userdata('admin_id'));

        if ($this->session->userdata('admin_auth')) {
            redirect('admin');
        }
        if ($url == 'confirm' && $this->input->is_ajax_request() == true) {
            if (DefuseLib::decrypt($admin->password) !== $this->input->post('pass_lama')) {
                echo goResult(false, "Opps! Maaf Password Anda Tidak Cocok");
                return;
            } else {
                $rules = [
                    'required' => [
                        ['password']
                    ],
                    'lengthMin' => [
                        ['password', 8]
                    ]
                ];
                $validate = Validation::check($rules, 'post');
                if (!$validate->auth) {
                    echo goResult(false, $validate->msg);
                    return;
                }
                if ($this->input->post('password') !== $this->input->post('repassword')) {
                    echo goResult(false, "Opps! Confirmation password not match.");
                    return;
                }

//                $ubah    			= $member;

                $admin->password = DefuseLib::encrypt($this->input->post('password'));

                $admin->save();

                echo goResult(true, "Ok! Password telah terupdate.");
            }


        } else {
            echo $this->blade->nggambar('website.admin.updatePassword', $data);
        }

    }

    // --------------------------------- START 	BLOG
    public function post($url = 'index', $id = null)
    {
        $data = $this->data;

        $data['menu'] = "blog";
        $data['category'] = CategoryBlogModel::desc()->get();
        $data['tag'] = TagModel::desc()->get();
        $data['profile'] = AdminModel::find($this->session->userdata('admin_id'));
        $data['blog'] = BlogModel::with('category')->where('author', $this->session->userdata('admin_id'))->desc()->get();


        if ($url == "create") {
            $data['type'] = "create";
            echo $this->blade->nggambar('website.admin.post.content', $data);
            return;
        } else if ($url == "created" && $this->input->is_ajax_request() == true) {

            $rules = [
                'required' => [
                    ['name'], ['description'], ['category']
                ]
            ];

            $validate = Validation::check($rules, 'post');

            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            $blog = new BlogModel;

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == false) {
                echo goResult(false, "Opss! Gambar Tidak Ada Atau Tidak Sesuai");
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == true) {

                $filename = 'BLOG__' . seo($this->input->post('name')) . '__' . date('Ymdhis');

                $upload = $this->upload('images/blog', 'image', $filename);
                if ($upload['auth'] == false) {
                    echo goResult(false, $upload['msg']);
                    return;
                }

                $blog->image = $upload['msg']['file_name'];
            }


            $blog->name = $this->input->post('name');
            $blog->description = $this->input->post('description');
            $blog->author = $this->session->userdata('admin_id');
            $blog->status = (null !== $this->input->post('status') ? 0 : 1);
            $blog->pilihan = (null !== $this->input->post('pilihan') ? 1 : 0);

            $category = CategoryBlogModel::find($this->input->post('category'));

            if (!isset($category->id)) {
                echo goResult(false, "Opps! Kategori Tidak Di Temukan");
            }
            $blog->id_category = $category->id;

            if ($blog->save()) {
                $tag = $this->input->post('tag');
                if ($tag) {
                    $tag = TagModel::whereIn('id', $tag)->desc()->get();
                    foreach ($tag as $result) {
                        $insertTag = new TagBlogModel;
                        $insertTag->id_blog = $blog->id;
                        $insertTag->id_tag = $result->id;
                        $insertTag->save();
                    }
                }

                echo goResult(true, "Sukses! , Blog Baru Telah Di Tambahkan");
                return;
            }
        } else if ($url == "update" && $id != null) {

            $data['blog'] = BlogModel::with('category', 'tags')->find($id);

            $data['blogtag'] = [];

            foreach ($data['blog']->tags as $result) {
                array_push($data['blogtag'], $result->id_tag);
            }

            $data['type'] = "update";

            if (!isset($data['blog']->id)) {
                redirect('admin/post');
                return;
            }
            if ($data['blog']->author != $this->session->userdata('admin_id')) {
                redirect('admin/post');
                return;
            }

            echo $this->blade->nggambar('website.admin.post.content', $data);
            return;
        } else if ($url == "updated" && $id != null && $this->input->is_ajax_request() == true) {

            $blog = BlogModel::find($id);

            if (!isset($blog->id)) {
                echo goResult(false, "Opss! Blog Tidak Di Temukan");
                return;
            }

            $rules = [
                'required' => [
                    ['name'], ['description'], ['category']
                ]
            ];

            $validate = Validation::check($rules, 'post');

            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == true) {

                $filename = 'BLOG__' . seo($this->input->post('name')) . '__' . date('Ymdhis');

                $upload = $this->upload('images/blog/', 'image', $filename);

                if ($upload['auth'] == false) {
                    echo goResult(false, $upload['msg']);
                    return;
                }

                if (!empty($upload['msg']['file_name'])) {
                    remFile(__DIR__ . '/../../public/images/blog/' . $blog->image);
                }

                $blog->image = $upload['msg']['file_name'];
            }

            $blog->name = $this->input->post('name');
            $blog->description = $this->input->post('description');
            $blog->status = (null !== $this->input->post('status') ? 0 : 1);
            $blog->pilihan = (null !== $this->input->post('pilihan') ? 1 : 0);

            $category = CategoryBlogModel::find($this->input->post('category'));
            if (!isset($category->id)) {
                echo goResult(false, "Opps! Kategori Tidak Di Temukan");
            }

            $blog->id_category = $category->id;

            if ($blog->save()) {

                $tag = $this->input->post('tag');

                if ($tag) {

                    TagBlogModel::where('id_blog', $blog->id)->delete();

                    $tag = TagModel::whereIn('id', $tag)->desc()->get();
                    foreach ($tag as $result) {
                        $insertTag = new TagBlogModel;
                        $insertTag->id_blog = $blog->id;
                        $insertTag->id_tag = $result->id;
                        $insertTag->save();
                    }
                }

                echo goResult(true, "Blog Telah Di Perbarui");
                return;
            }
        } else if ($url == "delete" && $id != null) {

            $blog = BlogModel::find($id);

            if (!isset($blog->id)) {
                redirect('admin/post');
                return;
            }

            TagBlogModel::where('id_blog', $blog->id)->delete();

            if ($blog->image != "") {
                if (file_exists("images/blog/{$blog->image}")) {
                    remFile(__DIR__ . '/../../public/images/blog/' . $blog->image);
                }
            }

            $blog->delete();

            redirect('admin/post');
        }
          else if ($url == "createTag") {
            $data['type'] = "create";
            echo $this->blade->nggambar('website.admin.post.content', $data);
            return;

        } else if ($url == "tagCreated" && $this->input->is_ajax_request() == true) {

              $rules = [
                  'required' => [
                      ['name'], ['description']
                  ]
              ];

              $validate = Validation::check($rules, 'post');

              if (!$validate->auth) {
                  echo goResult(false, $validate->msg);
                  return;
              }

              $tag = new TagModel;

              $tag->name = $this->input->post('name');
              $tag->description = $this->input->post('description');

              if ($tag->save()) {
                  echo goResult(true, "Tag Blog Telah Di Tambahkan");
                  return;
              }
          }else {
                  echo $this->blade->nggambar('website.admin.post.index', $data);
                  return;
              }
          }

// --------------------------------- END BLOG


    public function logout()
    {
        if (!$this->session->userdata('admin_auth')) {
            redirect('auth');
            return;
        }
        $this->session->unset_userdata('admin_auth');
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_name');
        redirect('admin');
    }

    private function validation($rules, $type)
    {
        if ($type == "post") {
            $v = new Valitron\Validator($_POST);
        } else {
            $v = new Valitron\Validator($_GET);
        }


        $v->rules($rules);
        if ($v->validate()) {
            return true;
        } else {
            return false;
        }
    }

    private function upload($dir, $name = 'userfile', $filename = false)
    {
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 3000;

        if ($filename) {
            $config['file_name'] = $filename;
        } else {
            $config['encrypt_name'] = TRUE;
        }

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
            $data['auth'] = false;
            $data['msg'] = $this->upload->display_errors();
            return $data;
        } else {
            $data['auth'] = true;
            $data['msg'] = $this->upload->data();
            return $data;
        }
    }

    private function isImage($file)
    {
        if ((($_FILES[$file]['type'] == 'image/gif') || ($_FILES[$file]['type'] == 'image/jpeg') || ($_FILES[$file]['type'] == 'image/png'))) {
            return true;
        } else {
            return false;
        }
    }

    private function upload_materi($dir, $name = 'userfile', $filename = false)
    {
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'doc|docx|jpg|png|jpeg|pdf';
        $config['max_size'] = 5000;

        if ($filename) {
            $config['file_name'] = $filename;
        } else {
            $config['encrypt_name'] = TRUE;
        }

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
            $data['auth'] = false;
            $data['msg'] = $this->upload->display_errors();
            return $data;
        } else {
            $data['auth'] = true;
            $data['msg'] = $this->upload->data();
            return $data;
        }
    }

    private function isDocument($file)
    {
        if ($_FILES[$file]['type'] == 'application/msword' || $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || $_FILES[$file]['type'] == 'application/pdf' || $_FILES[$file]['type'] == 'application/pdf') {

            return true;
        } else {
            return false;
        }
    }
}