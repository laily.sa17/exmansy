@extends('website.template')
@section('title')
	OPPS PAGE NOT FOUND!!!
@endsection
@section('styles')
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{base_url('assets')}}/css/error.css" />
@endsection

@section('content')
            <!-- 404-AREA START -->
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1>Oops!</h1>
                <h2>404 - The Page can't be found</h2>
            </div>
            <a href="#">Go TO Homepage</a>
        </div>
    </div>

            <!-- 404-AREA END -->   
@endsection


@section('script')

@endsection