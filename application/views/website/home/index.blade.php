@extends('website.template')
@section('title')
    {{$config->name}}
@endsection
@section('styles')
<style type="text/css">    .counter-section .nyoba .counter-item {
    min-width: 100px;
    font-size: 16px;
    text-transform: uppercase;
    color: #fff;
    border-right: 1px solid #35335a;
    text-align: center;
    display: inline-block;
}
.counter-section .nyoba .counter-item h4 {
      font-size: 36px;
     color: #fff;
      margin-bottom: 4px;
}

.counter-section .nyoba {
     padding-left: 40px;
}

</style>
@endsection

@section('script')
@endsection
@section('active-home')
active
@endsection
@section('content')
    <section class="hero-section">
       
        <div class="hero-slider owl-carousel">
                @foreach($slider as $result)
            <div class="hs-item set-bg" data-setbg="{{$result->imagedir}}">
                <div class="hs-text">

                </div>
            </div>
                @endforeach
        </div>
        
    </section>


    <section class="counter-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-6">
                    <div class="big-icon">
                        <i class="fa fa-graduation-cap"></i>
                    </div>
                    <div class="counter-content">
                        <h2>        {{$_SESSION['site_lang'] == 'english' ? $config->address_en : $config->address}} :</h2>
                        <p> {{$language['alamat']}}</p>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="nyoba">
                        <div class="counter-item"><h4><?php

echo  date("h");
?></h4>{{$language['jam']}}</div>
                        <div class="counter-item"><h4><?php

echo  date("i");
?></h4>{{$language['menit']}}</div>
                        <div class="counter-item"><h4><?php

echo  date("s");
?></h4>{{$language['detik']}}</div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="courses-section spad">
        <div class="container">
            <div class="section-title text-center">
                <h3>OUR COURSES</h3>
                <p>Building a better world, one course at a time</p>
            </div>
            <div class="row">
                <!-- course item -->
                <div class="col-lg-4 col-md-6 course-item">
                    <div class="course-thumb">
                        <img src="{{base_url('assets')}}/img/course/1.jpg" alt="">
                        <div class="course-cat">
                            <span>BUSINESS</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <div class="date"><i class="fa fa-clock-o"></i> 22 Mar 2018</div>
                        <h4>Certificate Course in Writing<br>for a Global Market</h4>
                        <h4 class="cource-price">$100<span>/month</span></h4>
                    </div>
                </div>
                <!-- course item -->
                <div class="col-lg-4 col-md-6 course-item">
                    <div class="course-thumb">
                        <img src="{{base_url('assets')}}/img/course/2.jpg" alt="">
                        <div class="course-cat">
                            <span>Marketing</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <div class="date"><i class="fa fa-clock-o"></i> 22 Mar 2018</div>
                        <h4>Google AdWords: Get More<br> Customers with Search Marketing </h4>
                        <h4 class="cource-price">$150<span>/month</span></h4>
                    </div>
                </div>
                <!-- course item -->
                <div class="col-lg-4 col-md-6 course-item">
                    <div class="course-thumb">
                        <img src="{{base_url('assets')}}/img/course/3.jpg" alt="">
                        <div class="course-cat">
                            <span>DESIGN</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <div class="date"><i class="fa fa-clock-o"></i> 22 Mar 2018</div>
                        <h4>The Ultimate Drawing Course<br> Beginner to Advanced</h4>
                        <h4 class="cource-price">$180<span>/month</span></h4>
                    </div>
                </div>
                <!-- course item -->
                <div class="col-lg-4 col-md-6 course-item">
                    <div class="course-thumb">
                        <img src="{{base_url('assets')}}/img/course/4.jpg" alt="">
                        <div class="course-cat">
                            <span>DATABASE</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <div class="date"><i class="fa fa-clock-o"></i> 22 Mar 2018</div>
                        <h4>Ultimate MySQL Bootcamp: Go from SQL Beginner to Expert</h4>
                        <h4 class="cource-price">$150<span>/month</span></h4>
                    </div>
                </div>
                <!-- course item -->
                <div class="col-lg-4 col-md-6 course-item">
                    <div class="course-thumb">
                        <img src="{{base_url('assets')}}/img/course/5.jpg" alt="">
                        <div class="course-cat">
                            <span>PROGRAM</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <div class="date"><i class="fa fa-clock-o"></i> 22 Mar 2018</div>
                        <h4>Web Developer Bootcamp<br>Make web  applications</h4>
                        <h4 class="cource-price">$250<span>/month</span></h4>
                    </div>
                </div>
                <!-- course item -->
                <div class="col-lg-4 col-md-6 course-item">
                    <div class="course-thumb">
                        <img src="{{base_url('assets')}}/img/course/6.jpg" alt="">
                        <div class="course-cat">
                            <span>BUSINESS</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <div class="date"><i class="fa fa-clock-o"></i> 22 Mar 2018</div>
                        <h4>How to Start an Amazon<br>FBA Store on a Tight Budget</h4>
                        <h4 class="cource-price">$150<span>/month</span></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="fact-section spad set-bg" data-setbg="{{base_url('assets')}}/img/fact-bg.jpg">
        <div class="container">
            <div class="row">
                 <div class="col-sm-6 col-lg-3 fact">
                    <div class="fact-icon">
                        <i class="ti-user"></i>
                    </div>
                    <div class="fact-text">
                        <h2>500</h2>
                        <p>{{$language['stu']}}</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3 fact">
                    <div class="fact-icon">
                        <i class="ti-crown"></i>
                    </div>
                    <div class="fact-text">
                        <h2>50</h2>
                        <p>{{$language['eks']}}</p>
                    </div>
                </div>
                 <div class="col-sm-6 col-lg-3 fact">
                    <div class="fact-icon">
                        <i class="ti-briefcase"></i>
                    </div>
                    <div class="fact-text">
                        <h2>80</h2>
                        <p>{{$language['pres']}}</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3 fact">
                    <div class="fact-icon">
                        <i class="ti-pencil-alt"></i>
                    </div>
                    <div class="fact-text">
                        <h2>800+</h2>
                        <p>{{$language['pris']}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="event-section spad">
        <div class="container">
            <div class="section-title text-center">
                <h3>OUR EVENTS</h3>
                <p>Our department  initiated a series of events</p>
            </div>
            <div class="row">
                <div class="col-md-6 event-item">
                    <div class="event-thumb">
                        <img src="{{base_url('assets')}}/img/event/1.jpg" alt="">
                        <div class="event-date">
                            <span>24 Mar 2018</span>
                        </div>
                    </div>
                    <div class="event-info">
                        <h4>The dos and don'ts of writing a personal<br>statement for languages</h4>
                        <p><i class="fa fa-calendar-o"></i> 08:00 AM - 10:00 AM <i class="fa fa-map-marker"></i> Center Building, Block A</p>
                        <a href="" class="event-readmore">REGISTER <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
                <div class="col-md-6 event-item">
                    <div class="event-thumb">
                        <img src="{{base_url('assets')}}/img/event/2.jpg" alt="">
                        <div class="event-date">
                            <span>22 Mar 2018</span>
                        </div>
                    </div>
                    <div class="event-info">
                        <h4>University interview tips:<br>confidence won't make up for flannel</h4>
                        <p><i class="fa fa-calendar-o"></i> 08:00 AM - 10:00 AM <i class="fa fa-map-marker"></i> Center Building, Block A</p>
                        <a href="" class="event-readmore">REGISTER <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="gallery-section">
        <div class="gallery">
            <div class="grid-sizer"></div>
            <div class="gallery-item gi-big set-bg" data-setbg="{{base_url('assets')}}/img/gallery/1.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/1.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item set-bg" data-setbg="{{base_url('assets')}}/img/gallery/2.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/2.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item set-bg" data-setbg="{{base_url('assets')}}/img/gallery/3.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/3.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item gi-long set-bg" data-setbg="{{base_url('assets')}}/img/gallery/5.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/5.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item gi-big set-bg" data-setbg="{{base_url('assets')}}/img/gallery/8.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/8.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item gi-long set-bg" data-setbg="{{base_url('assets')}}/img/gallery/4.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/4.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item set-bg" data-setbg="{{base_url('assets')}}/img/gallery/6.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/6.jpg"><i class="ti-plus"></i></a>
            </div>
            <div class="gallery-item set-bg" data-setbg="{{base_url('assets')}}/img/gallery/7.jpg">
                <a class="img-popup" href="{{base_url('assets')}}/img/gallery/7.jpg"><i class="ti-plus"></i></a>
            </div>
        </div>
    </div>


    <section class="blog-section spad">
        <div class="container">
            <div class="section-title text-center">
                <h3>LATEST NEWS</h3>
                <p>Get latest breaking news & top stories today</p>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    <div class="blog-item">
                        <div class="blog-thumb set-bg" data-setbg="{{base_url('assets')}}/img/blog/1.jpg"></div>
                        <div class="blog-content">
                            <h4>Parents who try to be their children’s best friends</h4>
                            <div class="blog-meta">
                                <span><i class="fa fa-calendar-o"></i> 24 Mar 2018</span>
                                <span><i class="fa fa-user"></i> Owen Wilson</span>
                            </div>
                            <p>Integer luctus diam ac scerisque consectetur. Vimus dot euismod neganeco lacus sit amet. Aenean interdus mid vitae sed accumsan...</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="blog-item">
                        <div class="blog-thumb set-bg" data-setbg="{{base_url('assets')}}/img/blog/2.jpg"></div>
                        <div class="blog-content">
                            <h4>Graduations could be delayed as external examiners</h4>
                            <div class="blog-meta">
                                <span><i class="fa fa-calendar-o"></i> 23 Mar 2018</span>
                                <span><i class="fa fa-user"></i> Owen Wilson</span>
                            </div>
                            <p>Integer luctus diam ac scerisque consectetur. Vimus dot euismod neganeco lacus sit amet. Aenean interdus mid vitae sed accumsan...</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="blog-item">
                        <div class="blog-thumb set-bg" data-setbg="{{base_url('assets')}}/img/blog/3.jpg"></div>
                        <div class="blog-content">
                            <h4>Private schools adopt a Ucas style application system</h4>
                            <div class="blog-meta">
                                <span><i class="fa fa-calendar-o"></i> 24 Mar 2018</span>
                                <span><i class="fa fa-user"></i> Owen Wilson</span>
                            </div>
                            <p>Integer luctus diam ac scerisque consectetur. Vimus dot euismod neganeco lacus sit amet. Aenean interdus mid vitae sed accumsan...</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="blog-item">
                        <div class="blog-thumb set-bg" data-setbg="{{base_url('assets')}}/img/blog/4.jpg"></div>
                        <div class="blog-content">
                            <h4>Cambridge digs in at the top of university league table</h4>
                            <div class="blog-meta">
                                <span><i class="fa fa-calendar-o"></i> 23 Mar 2018</span>
                                <span><i class="fa fa-user"></i> Owen Wilson</span>
                            </div>
                            <p>Integer luctus diam ac scerisque consectetur. Vimus dot euismod neganeco lacus sit amet. Aenean interdus mid vitae sed accumsan...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
