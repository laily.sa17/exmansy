@extends('website.template')
@section('title')
	Register - {{$config->name}}
@endsection

@section('content')
	  <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-main">
       <div class="cart wow bounceInUp animated">
          <div class="gap-sm"></div>
          <div class="row">
            <div class="col-sm-6 col-sm-push-3">
              <div class="box-register" style="margin-top: 20px;">
                <form id="form-login" method="post" action="#">
                  <div class="header">
                    <h3 style="color: orange; border-bottom:1px solid #eaeaea;margin: 0 0 15px 0;
                      padding: 25px 10px;">Daftar</h3>
                  </div>
                  <div class="gap-sm"></div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="text" placeholder="Nama Pengguna" required>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Ulangi Password" required>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-1">
                      <div class="checkbox">
                        <label style="margin-left: -70px;">
                          <input type="checkbox"><a style="font-size: 13px;">Dapatkan&nbsp;Artikel&nbsp;Terbaik<a href="{{base_url('main')}}/password/lupa" style="font-size: 13px; margin-left: 80px;">Lupa&nbsp;Password?</a></a>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <center>
                    <button type="submit" class="btn btn-danger btn-block">Daftar Sekarang</button>
                    </center>
                    <div class="gap-xs"></div>
                    <span class="help-block text-center" style="float: left;">
                      <a style="font-size: 13px; margin-left:50px;" >Sudah terdaftar ?</a>&nbsp;&nbsp;<a href="{{base_url('main')}}/member/login" style="font-size: 13px;"" class="text-primary"  onclick="showForm('forgot')">Login</a>
                    </span>
                  </div>
                </form>
            </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script')
<script src="https://apis.google.com/js/platform.js?onload=onLoadGoogleCallback" async defer></script>
 <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection