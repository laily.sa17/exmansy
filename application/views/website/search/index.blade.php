@extends('website.template')
@section('title')
    Searching '{{$ctrl->input->get('q')}}'
@endsection

@section('content')
<!-- Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="rows">
                <!-- Main content -->
                <div class="col col_12_of_12">
                    <!-- Content slider -->
                    
                    <!-- Panel title -->
                    <div class="panel_title">

                            <h4><a href="#">{{ ucwords($_GET['q']) }}</a></h4>
                        <hr>
                    </div>
                        </div>
                    </div><!-- End Panel title -->
                    <!-- Layout post 2 -->
                    <div class="row">
                        <div class="rows">
                            @foreach($news as $blog)
                                <div class="col col_4_of_12">
                                    <!-- Layout post 1 -->
                                    <div class="col col_12_of_12">
                                    <div class="card">
                                        <div class="layout_post_1">
                                            <div class="item_thumb">
                                                <div class="thumb_hover">
                                                    <a href="{{$blog->url}}"><img src="{{$blog->imagedir}}"  style="object-fit: cover;height: 250px;"></a>
                                                </div>
                                                <div class="thumb_meta">
                                                    @foreach($category->where('id', $blog->id_category) as $cate)
                                                        <span class="category" jQuery><a href="{{$blog->category->url}}">{{ $cate->name }}</a></span>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="item_content">
                                                <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                                <div class="item_meta clearfix">
                                                    <span><a href="{{ base_url('author') }}/{{ $blog->penulis->username }}"><i class="fa fa-user"></i> {{ $blog->penulis->name }}</a></span>
                                                    <span class="meta_date">{!! waktu_lalu($blog->created_at) !!}</span>
                                                    <span class="fa fa-eye">{!! k_view($blog->view) !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                            <!-- End Layout post 1 -->
                <ul class="page-numbers">
                        {{--{!! $pagination !!}--}}

                </ul><!-- End Pagination -->
                </div>
            </div>
        </div>
    </section><!-- End Section -->
@endsection


@section('script')

@endsection