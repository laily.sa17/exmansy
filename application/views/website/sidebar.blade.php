<!-- Widget Search widget -->
<div class="widget">
    <div class="widget_title"><h3>Search widget</h3></div>
    <div class="tb_widget_search">
        <div class="sub-search">
            <form action="{{base_url('main/search')}}" method="get">
                <input type="search" name="q" placeholder="Search...">
                <input type="submit" value="Search">
            </form>
        </div>
    </div>
</div><!-- End Widget Search widget -->
<!-- Widget top rated -->
<div class="widget">
    <div class="widget_title"><h3>Trending Topik</h3></div>
    <div class="tb_widget_top_rated clearfix">
        <!-- Post item -->
        @foreach($trending as $result)
            <div class="item clearfix">
                <div class="item_thumb clearfix">
                    <a href="{{ $result->url }}"><img class="newsPict" src="{{ $result->imagedir }}"></a>
                </div>
                <div class="item_content">
                    <h4><a href="{{ $result->url }}">{{ $result->name }}</a></h4>
                    <div class="item_meta clearfix">
                        <!-- <span class="meta_rating" title="Rated 4.50 out of 5">
                            <span jQuery>
                                <strong>4.50</strong>
                            </span>
                        </span> -->
                    </div>
                    <!-- <h4><a href="#">Nam at maximus nisl sed tempus est</a></h4> -->
                </div>
                <!-- <div class="order">1</div> -->
            </div>@endforeach<!-- End Post item -->
        <!-- Post item

    </div>
</div>End Widget top rated -->
        <!-- Widget Social widget -->
        <div class="widget">
            <div class="widget_title"><h3>Socialize</h3></div>
            <div class="tb_widget_socialize clearfix">

                @foreach($sosmed as $result)
                    <a href="{{$result->url}}" target="_blank" class="icon {{$result->type}}">
                        <div class="symbol" style="border-radius: 100%"><i class="fa fa-{{$result->type}}"
                                                                           style="line-height: 50px; "></i></div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div><!-- End Widget Social widget -->
<!-- Widget timeline -->
<div class="widget">
    <div class="widget_title"><h3>{{$events->name}}</h3></div>
    <div class="clearfix">
        <a href="{{$events->url}}" target="_blank"><img src="{{$events->imagedir}}" alt="{{$events->name}}"
                                                        title="{{$events->name}}">
        </a>
    </div>
</div>
<p></p>
<div class="widget">
    <div class="widget_title"><h3>Terbaru</h3></div>
    <div class="tb_widget_recent_list clearfix">
        <!-- Post item -->
        @foreach($terbaru as $blog)
            <div class="item clearfix">
                <div class="item_thumb">
                    <div class="thumb_hover">
                        <a class="hover_effect" href="{{$blog->url}}"><img src="{{$blog->imagedir}}">
                        </a>
                    </div>
                </div>
                <div class="item_content">
                    <h4><a href="{{$blog->category->url}}">
                            <span class="format" jQuery>{{$blog->category->name}}</span></a>
                        <a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                </div>
            </div>
    @endforeach<!-- End Post item -->

    </div>
</div><!-- End Widget Recent list posts -->
<!-- Widget posts -->
<div class="widget">
    <div class="widget_title"><h3>Popular</h3></div>
    <div class="tb_widget_posts_big clearfix">
        <!-- Post item -->
        <div class="item clearfix">
            <div class="item_content">
                <!-- Layout post 1 -->
                @foreach($popular as $blog)
                    <div class="col col_12_of_12">
                        <div class="card">
                            <div class="layout_post_2 clearfix">
                                <div class="item_thumb">
                                    <div class="thumb_hover">
                                        <a href="{{$blog->url}}">
                                            <source src="{{ $blog->name}}">
                                        </a>
                                    </div>
                                    <div class="thumb_meta">
                                        @foreach($category->where('id', $blog->id_category) as $cate)
                                            <span class="category" jQuery><a
                                                        href="{{$blog->category->url}}">{{ $cate->name }}</a></span>
                                        @endforeach
                                        <span class="comments">
                                        </span>
                                    </div>

                                </div>
                                <div class="item_content">
                                    <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>

                                    <div class="item_meta clearfix">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- End Layout post 1 -->
            </div>
        </div><!-- End Post item -->
    </div>
</div>
<div>

    <div class="widget">
        <div class="widget_title"><h3>Banner</h3></div>
        @if($dua->status == 1)

        @else
            @if(!empty($dua->video))<a href="{{$dua->link}}" data-popup="lightbox">
                <img src="{{$dua->video}}" alt="{{$dua->name}}" class="img-rounded img-preview"
                     style="object-fit: cover;width: 100%;">
            </a>
            @else
                <a href="{{$dua->link}}" data-popup="lightbox">
                    <img src="{{$dua->imagedir}}" alt="{{$dua->name}}" class="img-rounded img-preview"
                         style="object-fit: cover;width: 100%;">
                </a>
            @endif
        @endif
    </div>
</div>
<!-- Widget Tags -->
<div class="widget">
    <div class="widget_title"><h3>Tags</h3></div>
    <div class="tb_widget_tagcloud clearfix">
        @foreach($tag as $result)
            <a href="{{$result->url}}">{{$result->name}}</a>
        @endforeach
    </div>
</div><!-- End Widget Tags -->
</div><!-- @include('website.chat') -->
<!-- Widget Tags -->


