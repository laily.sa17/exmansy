@extends('website.template')
@section('title')
    NEWS - {{$config->name}}

    {{-- 	@if(isset($selected_tag))
            , TAG {{$selected_tag->name}}
        @endif --}}
@endsection

@section('content')
    <!-- Section -->
    <section><br><br><br><br>
        <div class="container">
            <!-- Main content -->
            <div class="col col_12_of_12">
                <div class="panel_title" jQuery>
                    <div>
                        <h4>NEWS</h4>
                    </div>
                </div>

            </div><!-- End Panel title -->
            <!-- Layout post 2 -->

            <div class="row">
                <div class="rows">

                    @foreach($news as $result)
                        <div class="col col_4_of_12">
                            <!-- Layout post 1 -->
                            <div class="col col_12_of_12">
                                <div class="card">
                                    <div class="layout_post_1">
                                        <div class="item_thumb">
                                            <div class="thumb_hover">
                                                <a href="{{$result->url}}"><img src="{{$result->imagedir}}"
                                                                                style="object-fit: cover;height: 250px;"></a></div>
                                            <div class="thumb_meta">
                                                @foreach($category->where('id', $result->id_category) as $cate)
                                                    <span class="category" jQuery><a
                                                                href="{{$result->category->url}}">{{ $cate->name }}</a></span>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="item_content">
                                            <h4><a href="{{$result->url}}">{{$result->name}}</a></h4>
                                            <div class="item_meta clearfix">
                                                 @if($result->author == 0)
                                                <span><a style="color: #00acedff" href="{{base_url('main/admin')}}"><i class="fa fa-user"></i> Admin</a></span>
                                            @else
                                                <span><a style="color: #00acedff"
                                                         href="{{ base_url('author') }}/{{ $result->penulis->username }}"><i
                                                                class="fa fa-user"></i> {{ $result->penulis->name }}</a></span>
                                            @endif
                                                <span class="meta_date">{!! waktu_lalu($result->created_at) !!}</span>
                                                <span class="fa fa-eye">{!! k_view($result->view) !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- End Layout post 1 -->
            <div class="container">
                <ul class="page-numbers">
                    {!! $pagination !!}
                </ul><!-- End Pagination -->
            </div>
        </div>

    </section><!-- End Section -->
@endsection