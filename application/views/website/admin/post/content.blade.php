@extends('website.admin.template')

@section('title')
    {{($type=="create") ? 'Buat Blog Baru' : 'Ubah Blog' }}
@endsection

@section('content')
    <div class="container">
        <div class="page-header">
            <div class="page-header-content">
                {{-- 		<div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Website</span> - Blog</h4>
                        </div> --}}
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a><i class="icon-magazine position-left"></i> Blog</a></li>
                    <li class="active">{{ ($type=="create") ? 'Buat Blog Baru' : 'Perbarui Blog' }}</li>
                </ul>
            </div>
        </div>

        <div class="content">
            <h6 class="content-group text-semibold">
                <small class="display-block">
                    {{ ($type=="create") ? 'Buat Blog Baru' : 'Perbarui Blog' }}
                </small>
            </h6>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title text-semibold">Data Blog</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 clearfix"
                             style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
                            <form id="form-blog" class="form-horizontal"
                                  action="{{ ($type=='create') ? base_url('admin/post/created') : base_url('admin/post/updated/'.$blog->id) }}"
                                  method="post">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Judul Blog <span class="text-danger"><b>*</b></span></label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" placeholder="Judul Blog" name="name"
                                               value="{{ ($type=='create') ? '' : $blog->name }}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Kategori Blog <span
                                                class="text-danger"><b>*</b></span></label>
                                    <div class="col-lg-10">
                                        <select class="select-search" name="category" required>
                                            <option value="">Pilih Kategori</option>
                                            @foreach($category as $result)
                                                @if($type=="update")
                                                    <option value="{{$result->id}}" {{($result->id==$blog->category->id) ? "selected" : ""}}>
                                                        {{$result->name}}
                                                    </option>
                                                @else
                                                    <option value="{{$result->id}}">{{$result->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tag Blog </label>
                                    <div class="col-lg-8">
                                        <select class="select-search" multiple="multiple" name="tag[]">
                                            <option value="">Pilih Tag</option>
                                            @foreach($tag as $result)
                                                @if($type=="update")
                                                    <option value="{{$result->id}}" {{(in_array($result->id,$blogtag)) ? "selected" : ""}}>
                                                        {{$result->name}}
                                                    </option>
                                                @else
                                                    <option value="{{$result->id}}">{{$result->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="col-lg-2">
                                        <button style="margin-top: 3px; width: 100%" class="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#exampleModal">
                                            Tambah Tag <i class="fa fa-plus position-right"></i>
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Deskripsi Blog <span
                                                class="text-danger"><b>*</b></span></label>
                                    <div class="col-lg-10">
                                        <textarea id="editor-full" rows="2" name="description"
                                                  cols="2">{!! ($type=='create') ? '' : $blog->description !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Gambar Kategori Blog<span class="text-danger"><b>*</b></span></label>
                                    <div class="col-lg-10 no-padding">
                                        <div class="col-md-12">
									<span class="help-block">
										Accepted formats: gif, png, jpg. Max file size 2Mb
                                        @if($type=="update")
                                            , Jangan Ganti Gambar Jika Tidak Ada perubahan
                                        @endif
									</span>
                                        </div>
                                        <div class="clearfix" style="margin-bottom: 20px;"></div>
                                        <div id="wrap-image">
                                            <div class="col-md-4">
                                                <div class="thumbnail" style="margin-bottom:10px;">
                                                    <div class="thumb">
                                                        <img src="{{ ($type=='create') ? img_holder() : $blog->imagedir }}"
                                                             class="product" alt="">
                                                        <div class="caption-overflow">
													<span>
														<a href="{{ ($type=='create') ? img_holder() : $blog->imagedir }}"
                                                           data-popup="lightbox" rel="gallery"
                                                           class="btn border-white text-white btn-flat btn-icon btn-rounded"><i
                                                                    class="icon-search4"></i></a>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="file" name="image" accept="image/*"
                                                           class="file-styled image-product-upload">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Status Blog<span class="text-danger"> <b>*</b></span></label>
                                    <div class="col-lg-10">
                                        <div class="checkbox checkbox-switch">
                                            <label>
                                                <input type="checkbox" name="status" value="0" class="switch"
                                                       data-on-text="Publish" data-off-text="Draft"
                                                       data-on-color="success"
                                                       data-off-color="default" {{ (@$blog->status==0) ? 'checked' : '' }} >
                                                Publikasi / Simpan Ke Draft
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Blog Pilihan<span
                                                class="text-danger"> <b>*</b></span></label>
                                    <div class="col-lg-10">
                                        <div class="checkbox checkbox-switch">
                                            <label>
                                                <input type="checkbox" name="pilihan" value="0" class="switch"
                                                       data-on-text="Ya" data-off-text="Tidak" data-on-color="success"
                                                       data-off-color="default" {{ (@$blog->pilihan==1) ? 'checked' : '' }} >
                                                Pilihan / Tampilkan Biasa
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit"
                                            class="btn btn-primary">{{ ($type=='create') ? 'Buat Blog' : 'Ubah Blog' }}
                                        <i class="icon-arrow-right14 position-right"></i></button>
                                    @if($type=="update")
                                        <a class="btn btn-danger" href="javascript:void(0)"
                                           onclick="window.history.back(); "> Batalkan <i
                                                    class="fa fa-times position-right"></i></a>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Tambah Tag Blog -->

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: white">
                        <h4 class="modal-title" id="exampleModalLabel">Buat Tag Blog Baru </h4>
                        <button style="margin-top: -2%" type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-tag" action="{{base_url('admin/post/tagCreated')}}">
                            <div class="form-group">
                                <br><br>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Nama Tag<span
                                                class="text-danger"><b>*</b></span></label>
                                    <div class="col-lg-8">
                                        <input class="form-control" type="text" placeholder="Nama Tag Blog" name="name"
                                               value="" required>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Deskripsi Tag Blog<span
                                                class="text-danger"><b>*</b></span></label>
                                    <div class="col-lg-8">
                                        <textarea name="description" placeholder="Deskripsi Tag Blog" cols="2"
                                                  class="form-control"></textarea>
                                        <br>
                                    </div>
                                </div>
                                <br>
                                <br><br><br>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Buat Tag<i
                                                class="icon-arrow-right14 position-right"></i></button>
                                    <a class="btn btn-danger" href="javascript:void(0)"
                                       onclick="window.history.back(); ">Batalkan
                                        <i class="fa fa-times position-right"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- /TAG -->

        @endsection

        @section('style')
            <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/core.css"/>
            <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/components.css"/>
            <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/colors.css"/>
            <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/icons/icomoon/styles.css"/>
        @endsection

        @section('script')

            <script type="text/javascript"
                    src="{{base_url()}}admin_assets/js/plugins/uploaders/fileinput.min.js"></script>
            <script type="text/javascript"
                    src="{{base_url()}}admin_assets/js/plugins/forms/selects/select2.min.js"></script>
            <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_layouts.js"></script>

            <script type="text/javascript">
                var editorsmall = false;
            </script>
            <script type="text/javascript" src="{{base_url()}}admin_assets/ckeditor/ckeditor.js"></script>
            <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/editor_ckeditor.js"></script>

            <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/uploader_bootstrap.js"></script>
            <script type="text/javascript"
                    src="{{base_url()}}admin_assets/js/plugins/forms/styling/switch.min.js"></script>
            <script type="text/javascript">
                $(".switch").bootstrapSwitch();

                $(".select-search").select2({
                    placeholder: "Pilih Tag Blog",
                    allowClear: true
                });

                $(".image-product-upload").change(function (e) {
                    var input = this;
                    var element = $(this).parents('.col-md-4').find('img');
                    var href = $(this).parents('.col-md-4').find('a[rel="gallery"]');

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            jQuery(element).attr('src', e.target.result);
                            jQuery(href).attr('href', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                })


                $("#form-blog").submit(function (e) {
                    e.preventDefault();
                    var formData = new FormData($("#form-blog")[0]);

                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }

                    $.ajax({
                        url: $("#form-blog").attr('action'),
                        method: "POST",
                        data: new FormData(this),
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            blockMessage($('#form-blog'), 'Please Wait , {{ ($type =="create") ? "Menambahkan Blog" : "Memperbarui Blog" }}', '#fff');
                        }
                    })
                        .done(function (data) {
                            $('#form-blog').unblock();
                            sweetAlert({
                                    title: ((data.auth == false) ? "Opps!" : 'sukses'),
                                    text: data.msg,
                                    type: ((data.auth == false) ? "error" : "success"),
                                },
                                function () {
                                    if (data.auth != false) {
                                        redirect("{{base_url('admin/post')}}");
                                        return;
                                    }
                                });

                        })
                        .fail(function () {
                            $('#form-blog').unblock();
                            sweetAlert({
                                title: "Opss!",
                                text: "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                                type: "error",
                            });
                        })

                })

            </script>
            <script type="text/javascript">
                $("#form-tag").submit(function (e) {
                    e.preventDefault();
                    var formData = new FormData($("#form-tag")[0]);

                    $.ajax({
                        url: $("#form-tag").attr('action'),
                        method: "POST",
                        data: new FormData(this),
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            blockMessage($('#form-tag'), 'Please Wait , Menambahkan Tag Blog', '#fff');
                        }
                    })
                        .done(function (data) {
                            $('#form-tag').unblock();
                            sweetAlert({
                                    title: ((data.auth == false) ? "Opps!" : 'Sukses'),
                                    text: data.msg,
                                    type: ((data.auth == false) ? "error" : "success"),
                                },
                                function () {
                                    if (data.auth != false) {
                                        redirect("{{base_url('admin/post')}}");
                                        return;
                                    }
                                });
                        })
                        .fail(function () {
                            $('#form-tag').unblock();
                            sweetAlert({
                                title: "Opss!",
                                text: "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                                type: "error",
                            });
                        })

                })

            </script>
@endsection
