
<!DOCTYPE>
<html>
<head>
    <title>@yield('title',$seo->title)</title>
    @yield('meta','<meta name="keywords" content="'.$seo->keyword.'">
    <meta name="description" content="'.$seo->description.'">
    <meta name="author" content="'.$config->name.'">
    <meta property="og:image" content="'.$seo->imagedir.'">')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="icon" href="{{$config->icondir}}">
        <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{{base_url('assets')}}/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{base_url('assets')}}/css/themify-icons.css"/>
    <link rel="stylesheet" href="{{base_url('assets')}}/css/magnific-popup.css"/>
    <link rel="stylesheet" href="{{base_url('assets')}}/css/animate.css"/>
    <link rel="stylesheet" href="{{base_url('assets')}}/css/owl.carousel.css"/>
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css"/>    <link rel="stylesheet" href="{{base_url('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/fontawesome.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/weather.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/colors.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/typography.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/author.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{base_url('assets/css/custom.css')}}">
<style typetype="text/css">
    #myBtn{
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #00aee3 ;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }
    #myBtn:hover{
        background-color: #005b9e ;
    }
</style>
    @yield('styles')

</head>
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- header section -->
    <header class="header-section">
        <div class="container">
            <!-- logo -->
            <a href="{{base_url('')}}" class="site-logo"><img src="{{$config->logodir}}" alt=""></a>
            <div class="nav-switch">
                <i class="fa fa-bars"></i>
            </div>
            <div class="header-info">
                <div class="hf-item">
                    <i class="fa fa-map-marker"></i>
                    <p><span>{{$_SESSION['site_lang'] == 'english' ? $config->address_en : $config->address}} : </span> {{$language['alamat']}} </p>
                </div>
                <div class="hf-item">
                    <i class="fa fa-language"></i>
                    <p><span>{{$language['bahasa']}} : </span>
            @if($_SESSION['site_lang'] == 'english')
                <a href="{{base_url('main')}}/switchLang/english" class="lang-active">English</a> 
                <a href="{{base_url('main')}}/switchLang/indonesian">|&nbsp;Indonesian</a>
            @else
                <a href="{{base_url('main')}}/switchLang/english">English</a> 
                <a href="{{base_url('main')}}/switchLang/indonesian" class="lang-active">|&nbsp;Indonesia</a>
            @endif</p>
                </div>
            </div>
        </div>
    </header>

    <nav class="nav-section">
        <div class="container">
            <div class="nav-right">
                 @foreach($menupengunjung as $menus)
                  @if($menus->id==10)
                <li class="@yield('active-login')"><a href="{{base_url($menus->link)}}">{{$menus->judul}}</a></li>
                @endif
                @endforeach
            </div>
            <ul class="main-menu">
                 @foreach($menupengunjung as $menus)
                 <?php
                 if ($_SESSION['site_lang'] == 'english')
                        $judul = $menus->judul_en;
                 else
                        $judul = $menus->judul;
                 ?>
                 @if($menus->id==1)
                <li class="@yield('active-home')"><a href="{{base_url($menus->link)}}">{{$judul}}</a></li>
                  @elseif($menus->id==2)
                <li  class="@yield('active-profile')"><a href="{{base_url($menus->link)}}">{{$judul}}</a></li>
                   @elseif($menus->id==3)
                <li class="@yield('active-eks')"><a href="{{base_url($menus->link)}}">{{$judul}}</a></li>
                   @elseif($menus->id==9)
                <li class="@yield('active-contact')"><a href="{{base_url($menus->link)}}">{{$judul}}</a></li>
                 @endif
                @endforeach
            </ul>
        </div>
    </nav>
@yield('content')

           
    <section class="newsletter-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-7">
                    <div class="section-title mb-md-0">
                    <h3>EXMANSY</h3>
                    <p>{{$language['kata']}}</p>
                </div>
                </div>
                <div class="col-md-7 col-lg-5">
                    <form class="newsletter">
                       <a href="http://www.smkn1-sby.sch.id/"> <input type="text" placeholder="{{$language['kata1']}}"></a>
                        <button class="site-btn"><a href="http://www.smkn1-sby.sch.id/" style="color: #fff;">{{$language['kata2']}}</a></button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-section">

        <div class="copyright">
            <div class="container">
                <p>
{{$language['kata3']}} &copy;<script>document.write(new Date().getFullYear());</script>&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp; by <a href="http://www.smkn1-sby.sch.id/" target="_blank">IT Support</a>
</p>
            </div>      
        </div>
    </footer>

<button onclick="topFunction()" id="myBtn"><i class="fa fa-arrow-up" style="color: white;"></i></button>
<!-- Script -->
    <script src="{{base_url('assets')}}/js/jquery-3.2.1.min.js"type="text/javascript" ></script>
    <script src="{{base_url('assets')}}/js/owl.carousel.min.js"type="text/javascript" ></script>
    <script src="{{base_url('assets')}}/js/jquery.countdown.js"type="text/javascript" ></script>
    <script src="{{base_url('assets')}}/js/masonry.pkgd.min.js"type="text/javascript" ></script>
    <script src="{{base_url('assets')}}/js/magnific-popup.min.js"type="text/javascript" ></script>
    <script src="{{base_url('assets')}}/js/main.js"type="text/javascript" ></script>
    <script type="text/javascript" src="{{base_url('assets')}}/js/jqueryscript.min.js"></script>
    <script type="text/javascript" src="{{base_url('assets')}}/js/smoothscroll.min.js"></script>
    <script type="text/javascript">
        var mybutton=document.getElementById("myBtn");
        window.onscroll=function(){
            scrollFunction()
        };
        function scrollFunction(){
            if (document.body.scrollTop>20 || document.documentElement.scrollTop>20) {
                mybutton.style.display="block";
            }
            else{
                mybutton.style.display="none";
            }
        }


        function topFunction(){
            document.body.scrollTop=0;
            document.documentElement.scrollTop=0;
        }
    </script>
@yield('script')
</body>

</div>
</footer><!-- End Footer -->
<!-- Copyright -->

</html>