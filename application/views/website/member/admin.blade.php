@extends('website.template')

@section('title')
    Member
@endsection

@section('style')
    <link rel="stylesheet" href="{{base_url('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/fontawesome.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/colors.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/typography.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css">
    <link rel="stylesheet" type="text/css" media="(max-width:768px)" href="{{base_url('assets')}}/css/responsive-0.css">
    <link rel="stylesheet" type="text/css" media="(min-width:769px) and (max-width:992px)" href="{{base_url('assets')}}/css/responsive-768.css">
    <link rel="stylesheet" type="text/css" media="(min-width:993px) and (max-width:1200px)" href="{{base_url('assets')}}/css/responsive-992.css">
    <link rel="stylesheet" type="text/css" media="(min-width:1201px)" href="{{base_url('assets')}}/css/responsive-1200.css">
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>
@endsection

@section('content')
    <div class="container">
        <div class="col-md-2"><br><br><br>
            <img class="img-responsive" src="{{base_url()}}{{img_holder('profile')}}" align="middle  "style="width:126.66px;height:126.66px;" >
        </div>
        <div class="col-md-10">
            <br><br><br>
            <h2>Member</h2>
            <h3><span class="text-muted small">{{ count($news) }} ARTICLES</span></h3>
        </div>
        <div class="col-md-12">
            <div class="panel_title"><br>
                <div>

                    <h4><a href="blog.html">Artikel </a></h4>
                </div>
            </div><!-- End Panel title -->
        </div>
        <div class="row">
            <div class="rows">
                @foreach($news as $blog)
                    <div class="col col_4_of_12">
                        <!-- Layout post 1 -->
                        <div class="card">
                            <div class="layout_post_1">
                                <div class="item_thumb">
                                    <div class="thumb_hover">
                                        <a href="{{$blog->url}}"><img src="{{$blog->imagedir}}"  style="object-fit: cover;height: 250px;"></a>
                                    </div>
                                    <div class="thumb_meta">
                                        @foreach($category->where('id', $blog->id_category) as $cate)
                                            <span class="category" jQuery><a href="{{$blog->category->url}}">{{ $cate->name }}</a></span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="item_content">
                                    <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                    <p>{!! read_more($blog->description,200) !!}</p>
                                    <div class="item_meta clearfix">
                                        <span><a href="#"><i class="fa fa-user"></i>Member</a></span>
                                        <span class="meta_date">{!! waktu_lalu($blog->created_at) !!}</span>
                                        <span class="fa fa-eye">{!! k_view($blog->view) !!}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection