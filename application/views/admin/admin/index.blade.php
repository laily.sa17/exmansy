@extends('admin.template')

@section('title')
 Daftar Akun Admins
@endsection

@section('content')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Website</span> - Admins</h4>
		</div>

	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{base_url('superuser/admin')}}"><i class="icon-user position-left"></i> Admins</a></li>
			<li class="active">Data Admins</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-user"></i> Daftar</span> Admin
		<small class="display-block">Ini Merupakan Daftar Akun Admin Yang Telah Anda Buat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Daftar Akun Admin Anda</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;">
					<a href="{{base_url('superuser/admin/create')}}" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-plus3"></i></b> Tambah Admin Baru</a>
				</div>
				   <div class="col-md-12" style="margin-top:30px;margin-bottom:10px;">
			        <form id="bulkaction" method="post" action="{{base_url('superuser/admin/action')}}">
			            <div class="col-xs-1">
			                <div class="checkbox">
			                    <label>
			                        <input type="checkbox" class="styled" onchange="checkData(this)" name="toggleCheck">
			                        Check
			                    </label>
			                </div>
			            </div>
			            <div class="col-xs-2">
			                <select class="form-control" name="action" required>
			                    <option value="">Aksi</option>
			                    <option value="delete">Hapus Admin</option>
			                </select>
			            </div>
			            <div class="col-xs-8">
			                <button type="submit" class="btn bg-danger btn-sm"><i class="icon-check"></i> Action</button>
			            </div>
			        </form>
			        <div class="clearfix"></div>
		        		<div class="gap-md"></div>
			    </div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No</th>
			            	<th><></th>
                        	<th>Avatar</th>
                            <th>Admin</th>
                            <th>Info</th>
                            <th>Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($admin as $key => $result)
                         <tr>
                        	<td align="center">{{($key+1)}}</td>
                       <td >
			                    <div class="checkbox">
			                        <label>
			                            <input type="checkbox" form="bulkaction" value="{{$result->id}}" class="styled data-check" name="data[]">
			                        </label>
			                    </div>
			                </td>
                        	 <td>
		                        <a href="{{$result->imagedir}}" data-popup="lightbox">
			                        <img src="{{$result->imagedir}}" alt="" class="img-rounded img-preview" style="object-fit: cover;width: 100%;height: 70px;">
		                        </a>
	                        </td>
	                        <td style="width:300px;">
	                        	<a href="{{base_url('superuser/admin/update/'.$result->id.'/'.seo($result->name))}}">
	                        		<b>{{ucwords(read_more($result->name,30))}}</b>
	                        	</a><br>
	                        	<span class="text-size-mini">
	                        	Tanggal Di Buat : {{tgl_indo($result->created_at)}}
	                        	</span>
	                        </td>
	                         <td  align="left">
	                        	<span class="text-size-mini">
	                        	Terakhir : <span class="text-info">{{tgl_indo($result->lastlog)}}</span>
	                        	</span><br>
	                        	<span class="text-size-small">
	                        		ip address : <span class=" text-info">{{$result->ipaddress}}</span>
	                        	</span>
	                        </td>
	                        <td align="center">
	                        	<div class="checkbox checkbox-switch">
									<label>
										<input type="checkbox" name="status" data-size="mini" value="0" class="switch" 
										data-on-text="On" data-off-text="Off" data-on-color="success" 
										data-off-color="danger" onchange="changeStatus(this)" data-id="{{$result->id}}"
										 {{(@$result->status=='active') ? 'checked' : ''}} >
									</label>
								</div>
	                        </td>	                      
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a href="{{base_url('superuser/admin/update/'.$result->id.'/'.seo($result->name))}}">
												<i class="fa fa-edit"></i> Ubah Administrator
											</a>
										</li>
										<li><a href="javascript:void(0)" onclick="deleteIt(this)" 
										data-url="{{base_url('superuser/admin/delete/'.$result->id.'/'.seo($result->name))}}">
												<i class="fa fa-trash"></i> Hapus Administrator
											</a>
										</li>
									</ul>
								</div>
	                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/media/fancybox.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_layouts.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/styling/switch.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/gallery_library.js"></script>
<script type="text/javascript">
	$(".switch").bootstrapSwitch();	

	 function checkData(that) {
	 	if($(that).is(':checked')){
	 		$("input[name='data[]']").prop('checked',true);
	 	}
	 	else {
	 		$("input[name='data[]']").prop('checked',false);
	 	}

	 	$.uniform.update();
	 }

	 $("#bulkaction").submit(function(e){
			e.preventDefault();
			if($("input[name='data[]']:checked").length <= 0 ){
				return;
			}
			if($("select[name=action]").val()=="status"){

			 	var html 		= '<div class="col-xs-10 margin-center">'+
			 						'<select class="form-control" name="status">'+
			 							'<option value="register">REGISTER</option>'+
			 							'<option value="active">ACTIVE</option>'+
			 							'<option value="blocked">BLOCKED</option>'+
			 					   '</select>'+
			 					   '</div>';			 	
			 	swal({
				  title: "Ubah Status Admin",
				  text: html,
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, Change It!",
				  closeOnConfirm: false,
				  html: true
				},
				function(){
					var data = $("#bulkaction").serializeArray();
					
					data.push({name:'status' ,value: $('select[name=status]').val()});
					console.log(data);
					$.ajax({
						url: "{{base_url('superuser/admin/action')}}",
						method: 	"POST",
						data: data,
						beforeSend: function(){
							blockMessage('.panel','Please Wait , Procesing Data','#fff');		
						}
					})
					.done(function(data){
						$('.panel').unblock();
						sweetAlert({
							title: 	((data.auth==false) ? "Opps!" : 'Success' ),
							text: 	data.msg,
							type: ((data.auth==false) ? "error" : 'success' ),
						},
						function(){
							redirect("{{base_url('superuser/admin')}}");		
							return;
						});

					})
					.fail(function() {
					    $('.panel').unblock();
						sweetAlert({
							title: 	"Opss!",
							text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
							type: 	"error",
						},
						function(){

						});
					 })
				  
				});
			}
			else if($("select[name=action]").val()=="delete"){

			 	swal({
				  title: "Peringatan",
				  text: 'Anda Yaking Ingin Menghapus Data Ini ? ',
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, Change It!",
				  closeOnConfirm: false,
				},
				function(){
					var data = $("#bulkaction").serializeArray();
					$.ajax({
						url: "{{base_url('superuser/admin/action')}}",
						method: 	"POST",
						data: data,
						beforeSend: function(){
							blockMessage('.panel','Please Wait , Procesing Data','#fff');		
						}
					})
					.done(function(data){
						$('.panel').unblock();
						sweetAlert({
							title: 	((data.auth==false) ? "Opps!" : 'Success' ),
							text: 	data.msg,
							type: ((data.auth==false) ? "error" : 'success' ),
						},
						function(){
							redirect("{{base_url('superuser/admin')}}");		
							return;
						});

					})
					.fail(function() {
					    $('.panel').unblock();
						sweetAlert({
							title: 	"Opss!",
							text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
							type: 	"error",
						},
						function(){

						});
					 })
				  
				});
			}
			else if($("select[name=action]").val()=="message"){

				var data 				= $("input[name='data[]']:checked");
				var modal 				= $("#modal-message");

				$.each(data , function(i, that) { 
					console.log($(that).val());
					$("#product-recieve option[value="+$(that).val()+"]").prop('selected',true);
				});

				$("select.select-search").select2({
					placeholder: "Pilih Penerima",
			  		allowClear: true
				});

				$(modal).modal('show');
			}
		})
	function changeStatus(that){
		var id 		= $(that).attr('data-id');
		var status 	= ($(that).is(":checked")) ? 'active' : 'blocked';
		
		$.ajax({
			url: "{{base_url('superuser/admin/changestatus')}}",
			method: 	"POST",
			data: {id:id,status:status},
			beforeSend: function(){
				blockMessage('.panel','Please Wait , Procesing Data','#fff');		
			}
		})
		.done(function(data){
			$('.panel').unblock();

			if(data.auth==false){
				sweetAlert({
					title: 	"Opps!",
					text: 	data.msg,
					type: "error",
				});
			}
		})
		.fail(function() {
		    $('.panel').unblock();
			sweetAlert({
				title: 	"Opss!",
				text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
				type: 	"error",
			});
		 })
	}
</script>
@endsection
