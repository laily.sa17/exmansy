@extends('admin.template')

@section('title')
 Daftar Gallery
@endsection

@section('content')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Website</span> - Gallery</h4>
		</div>

	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{base_url('superuser/gallery')}}"><i class="icon-images3 position-left"></i> Gallery</a></li>
			<li class="active">Data Gallery</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-images3"></i> Daftar</span> Gallery
		<small class="display-block">Ini Merupakan Daftar Gallery Yang Telah Anda Buat</i></small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Daftar Gallery Anda</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;">
					<a href="{{base_url('superuser/gallery/create')}}" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-plus3"></i></b> Tambah Gallery Baru</a>
				</div>
				s<div class="col-md-12" style="margin-top:30px;margin-bottom:10px;">
			        <form id="bulkaction" method="post" action="{{base_url('superuser/gallery/action')}}">
			            <div class="col-xs-1">
			                <div class="checkbox">
			                    <label>
			                        <input type="checkbox" class="styled" onchange="checkData(this)" name="toggleCheck">
			                        Check
			                    </label>
			                </div>
			            </div>
			            <div class="col-xs-2">
			                <select class="form-control" name="action" required>
			                    <option value="">Aksi</option>
			                    <option value="delete">Hapus Gallery</option>
			                </select>
			            </div>
			            <div class="col-xs-8">
			                <button type="submit" class="btn bg-danger btn-sm"><i class="icon-check"></i> Action</button>
			            </div>
			        </form>
			        <div class="clearfix"></div>
		        		<div class="gap-md"></div>
			    </div>
				<table class="table table-striped media-library table-lg table-responsive">
                    <thead>
                        <tr>
                        	<th>No</th>
                        	<th><></th>
                        	<th>Gambar</th>
                            <th>Gallery</th>
                            <th>Tipe</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($gallery as $key => $result)
                         <tr>
                        	<td align="center">{{($key+1)}}</td>
                        	                       <td >
			                    <div class="checkbox">
			                        <label>
			                            <input type="checkbox" form="bulkaction" value="{{$result->id}}" class="styled data-check" name="data[]">
			                        </label>
			                    </div>
			                </td>
	                        <td>
		                        <a href="{{$result->imagedir}}" data-popup="lightbox">
			                        <img src="{{$result->imagedir}}" alt="" class="img-rounded img-preview" style="object-fit: cover;width: 100%;height: 70px;">
		                        </a>
	                        </td>
	                        <td style="width:500px;">
	                        	<a href="{{$result->urlupdate}}">
	                        		<b>{{ucwords(read_more($result->name,100))}}</b>
	                        	</a><br>
	                        	<span class="text-size-mini">
	                        	Tanggal Publish : {{tgl_indo($result->created_at)}}
	                        	</span><br>
	                        	<span class="text-size-small text-muted">
	                        		{{read_more($result->description,120)}}
	                        	</span>
	                        </td>
	                        <td align="center">
	                        	@if($result->type=="image")
	                        		<span class="label label-primary">GAMBAR</span>
	                        	@else
	                        		<span class="label label-danger">VIDEO</span>
	                        	@endif
	                        </td>
	                      
	                        <td class="text-center">
	                           <div class="btn-group">
			                    	<button type="button" class="btn btn-danger btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown"><i class="icon-cog5 position-left"></i> Action <span class="caret"></span></button>
			                    	<ul class="dropdown-menu dropdown-menu-right">
			                    		<li>
											<a href="{{$result->url}}" target="_blank">
												<i class="fa fa-eye"></i> Lihat Gallery Website
											</a>
										</li>
										<li>
											<a href="{{$result->urlupdate}}">
												<i class="fa fa-edit"></i> Ubah Gallery
											</a>
										</li>
										<li><a href="javascript:void(0)" onclick="deleteIt(this)" 
										data-url="{{$result->urldelete}}">
												<i class="fa fa-trash"></i> Hapus Gallery
											</a>
										</li>
									</ul>
								</div>
	                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/media/fancybox.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_layouts.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/styling/switch.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/default_datatable.js"></script>
<script type="text/javascript">
		$(".switch").bootstrapSwitch();	
		 function checkData(that) {
	 	if($(that).is(':checked')){
	 		$("input[name='data[]']").prop('checked',true);
	 	}
	 	else {
	 		$("input[name='data[]']").prop('checked',false);
	 	}

	 	$.uniform.update();
	 }

	 $("#bulkaction").submit(function(e){
			e.preventDefault();
			if($("input[name='data[]']:checked").length <= 0 ){
				return;
			}
			if($("select[name=action]").val()=="status"){

			 	var html 		= '<div class="col-xs-10 margin-center">'+
			 						'<select class="form-control" name="status">'+
			 							'<option value="register">REGISTER</option>'+
			 							'<option value="active">ACTIVE</option>'+
			 							'<option value="blocked">BLOCKED</option>'+
			 					   '</select>'+
			 					   '</div>';			 	
			 	swal({
				  title: "Ubah Status Gallery",
				  text: html,
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, Change It!",
				  closeOnConfirm: false,
				  html: true
				},
				function(){
					var data = $("#bulkaction").serializeArray();
					
					data.push({name:'status' ,value: $('select[name=status]').val()});
					console.log(data);
					$.ajax({
						url: "{{base_url('superuser/gallery/action')}}",
						method: 	"POST",
						data: data,
						beforeSend: function(){
							blockMessage('.panel','Please Wait , Procesing Data','#fff');		
						}
					})
					.done(function(data){
						$('.panel').unblock();
						sweetAlert({
							title: 	((data.auth==false) ? "Opps!" : 'Success' ),
							text: 	data.msg,
							type: ((data.auth==false) ? "error" : 'success' ),
						},
						function(){
							redirect("{{base_url('superuser/gallery')}}");		
							return;
						});

					})
					.fail(function() {
					    $('.panel').unblock();
						sweetAlert({
							title: 	"Opss!",
							text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
							type: 	"error",
						},
						function(){

						});
					 })
				  
				});
			}
			else if($("select[name=action]").val()=="delete"){

			 	swal({
				  title: "Peringatan",
				  text: 'Anda Yaking Ingin Menghapus Data Ini ? ',
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, Change It!",
				  closeOnConfirm: false,
				},
				function(){
					var data = $("#bulkaction").serializeArray();
					$.ajax({
						url: "{{base_url('superuser/gallery/action')}}",
						method: 	"POST",
						data: data,
						beforeSend: function(){
							blockMessage('.panel','Please Wait , Procesing Data','#fff');		
						}
					})
					.done(function(data){
						$('.panel').unblock();
						sweetAlert({
							title: 	((data.auth==false) ? "Opps!" : 'Success' ),
							text: 	data.msg,
							type: ((data.auth==false) ? "error" : 'success' ),
						},
						function(){
							redirect("{{base_url('superuser/gallery')}}");		
							return;
						});

					})
					.fail(function() {
					    $('.panel').unblock();
						sweetAlert({
							title: 	"Opss!",
							text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
							type: 	"error",
						},
						function(){

						});
					 })
				  
				});
			}
			else if($("select[name=action]").val()=="message"){

				var data 				= $("input[name='data[]']:checked");
				var modal 				= $("#modal-message");

				$.each(data , function(i, that) { 
					console.log($(that).val());
					$("#product-recieve option[value="+$(that).val()+"]").prop('selected',true);
				});

				$("select.select-search").select2({
					placeholder: "Pilih Penerima",
			  		allowClear: true
				});

				$(modal).modal('show');
			}
		})
</script>
@endsection
