<?php 
class Aksa_pagination {
	
	function __construct(){
		$this->ci =&get_instance();
		$this->ci->load->library('pagination');
	}

	function paginate($url, $numlinks, $per_page, $number_row,$page=0){

		# iki nggolek onok pirang page
		# misal onok 
		$total_rows = ceil($number_row/$per_page);

		$config = [
		'base_url' => $url,
			'total_rows' => $total_rows,
			'per_page' => 1,
			//'uri_segment' => $uri_segment,
			'first_link' => false,
			'last_link' => false,
					//'uri_segment' => $uri_segment,
			'next_link'        => '&raquo;',
			'prev_link'        => '&laquo;',
			'full_tag_open'    => '<ul class="page-numbers">',
			'full_tag_close'   => '</ul>',
			'num_tag_open'     => '<li><span class="page-numbers">',
			'num_tag_close'   	=> '</span></li>',
			'cur_tag_open'     => '<li><span class="page-numbers current">',
			'cur_tag_close'    => '</span></li>',
			'next_tag_open'    => '<li><span class="page-numbers">',
			'next_tag_close'  => '</span></li>',
			'prev_tag_open'    => '<li><span class="page-numbers">',
			'prev_tag_close'  => '</span></li>',

			'num_links'		   => $numlinks,
			'first_tag_open'   => '<li><span class="page-numbers">',
			'first_tag_close' => '</span></li>',
			'last_tag_open'    => '<li><span class="page-numbers">',
			'last_tag_close'  => '</span></li>'
		];

		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);

		$this->ci->pagination->initialize($config);
		return $this->ci->pagination->create_links();

	}
}