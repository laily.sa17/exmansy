<?php

class SiswaModel extends MY_Model
{
	protected $table 	= "siswa";
	protected $appends 	= array('urlupdate','urldelete');


	public function getUrlupdateAttribute()
	{
		return base_url('superuser/siswa/update/'.$this->id.'/'.seo($this->name));
	}

	public function getUrldeleteAttribute()
	{
		return base_url('superuser/siswa/delete/'.$this->id);
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}

}
