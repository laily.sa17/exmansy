<?php

class MemberModel extends MY_Model
{
	protected $table 	= "member";
	protected $appends 	= array();
		public function getImagedirAttribute()
	{
		if (!$this->image|| !file_exists("images/member/{$this->image}")) {
			return base_url().img_holder();
		}

		return base_url("images/member/{$this->image}");	
	}
		
	public function blog()
	{
		return $this->hasMany('BlogModel', 'author', 'id');
	}

	public function scopeStatus($query,$status){
		return $query->where("status",$status);
	}

}
