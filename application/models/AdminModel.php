<?php

class AdminModel extends MY_Model
{
	protected $table 	= "admin";
	protected $appends 	= array();
		public function getImagedirAttribute()
	{
		if (!$this->image|| !file_exists("images/admin/{$this->image}")) {
			return base_url().img_holder();
		}

		return base_url("images/admin/{$this->image}");	
	}
		public function getLogodirAttribute()
	{
		if (!$this->logo|| !file_exists("images/admin/{$this->logo}")) {
			return base_url().img_holder();
		}

		return base_url("images/admin/{$this->logo}");	
	}
	public function blog()
	{
		return $this->hasMany('BlogModel', 'author', 'id');
	}

	public function scopeStatus($query,$status){
		return $query->where("status",$status);
	}

}
