<?php
$lang['bahasa']      = 'LANGUAGE';
$lang['alamat']      = 'Jl Smea No.4, Surabaya City, East Java, Indonesian';
$lang['kata']        = 'Develop Your Talent And Achievement With Us';
$lang['kata1']       = "Lets's";
$lang['kata2']       = "JOIN US !";
$lang['kata3']       = "Copyright";
$lang['detik']        = "Sec";
$lang['jam']         = "Hrs";
$lang['menit']       = "Mins";
// home index
$lang['eks']       	 = "Extracurricular";
$lang['pres']        = "Achievement";
$lang['pris']        = "Event";
$lang['stu']       	 = "Students";

$lang['home']        = 'Home';
$lang['aboutus']   	 = 'About Us';
$lang['about']   	 = 'About Zafer';
$lang['article']	 = 'Article';
$lang['article_t']	 = 'Related Article';
$lang['ourArticle']	 = 'Our Article';
$lang['product']	 = 'Product';
$lang['ourProduct']	 = 'Our Product';
$lang['gallery']     = 'Gallery';
$lang['ourGallery']  = 'Our Gallery';
$lang['vision']      = 'Vision & Mission';
$lang['video']       = 'Video';
$lang['picture']     = 'Picture';
$lang['contact']     = 'Contact Us';
$lang['contactVia']  = 'Please Contact Us Via:';
$lang['read_more']   = 'Read More';
$lang['see_more']    = 'See More';
$lang['more']        = 'View More';
$lang['detail']      = 'Details';
$lang['category']    = 'Category';
$lang['recent']      = 'Recent Post';
$lang['testimoni']      = 'Testimonials';
$lang['visi']      = 'Vision';
$lang['misi']      = ' Mission';
//    Cart Page
$lang['cart']        = 'Cart';
$lang['preorder']        = 'Preorder';
$lang['yourCart']    = 'Your Shopping Cart';
$lang['shopNow']     = 'Go Shopping';
$lang['clearAll']    = 'Clear All';
$lang['clear']       = 'Remove From Cart';
$lang['productN']    = 'Product Name';
$lang['price']       = 'Price';
$lang['action']      = 'Action';
$lang['productT']    = 'Total Products Will Be Purchased';
$lang['booking']     = 'Booking';
$lang['day']         = 'Day';
$lang['aPurchase']   = 'After Puchase';

//    Contact Page
$lang['criticsm']    = 'Criticism and Suggestions';
$lang['cName']       = 'Your Name';
$lang['cEmail']      = 'Your Email';
$lang['cPhone']      = 'Your Phone Number';
$lang['cMessage']    = 'Description';
$lang['send']		 =	'Send';
$lang['contact']	 =	'Contact';
$lang['us']	 =	'Us';
$lang['os']	 =	'Store';
$lang['our']	 =	'Our';
$lang['us']	 =	'Us';
//    Custom Page
$lang['csExplain']   = 'If you want to make a custom jacket model with your own design, please fill out the form below:';
$lang['csName']      = 'Full Name';
$lang['csName_2']    = 'Enter your name';
$lang['csMale']      = 'Male';
$lang['csFemale']    = 'Female';
$lang['csTitle']     = 'Data Custom Order';
$lang['csImage']     = 'Image Order';
$lang['csAddress']   = 'Address';
$lang['csAddress_2'] = 'Enter Your Address';
$lang['csPhone']     = 'Phone Number';
$lang['csPhone_2']   = 'Enter Your Phone Number';
$lang['csEmail']     = 'Email';
$lang['csEmail_2']   = 'Enter Yor Email';
$lang['csDesc']      = 'Description Order';
$lang['csDesc_2']    = 'Enter Description Order';
$lang['csGender']    = 'Gender';
$lang['send']    = 'Send';
$lang['create']    = 'CREATE YOUR OWN';
// footer
$lang['bantuan']='Help';
$lang['fu']='Follow Us';
//product detail
 $lang['desc']    = 'Description';
$lang['col']    = 'Color';
$lang['size']    = 'Size';
// checkout sukses
$lang['thanks_a']    = 'Terima Kasih Telah Berbelanja Kepada Kami';
$lang['thanks_b']    = 'We are very grateful to you for shopping and trusting us as sellers, your transaction is being processed,
             please check your inbox / spam for your email to find out the next step,
             You have not received an email from us?';
$lang['thanks_c']    = 'Resend Email';
$lang['thanks_d']    = 'You can see / check your transaction status on the following page';
$lang['thanks_e']    = 'Track Transactions';
$lang['thanks_f']    = 'If there are things you want to ask, please';
$lang['status']    = 'Transaction Status';
$lang['order']    = 'ORDER';
$lang['request']    = 'REQUEST';
$lang['approve']    = 'APPROVE';
$lang['packing']    = 'PACKING';
$lang['delivery']    = 'DELIVERY';
$lang['done']    = 'DONE';
$lang['cancel']    = 'CANCEL';
$lang['nomor_i']    = 'Number Invoice';
$lang['tanggal']    = 'Date';
$lang['kadaluarsa']    = 'Expired';
$lang['status_a']    = 'Payment Status';
$lang['meminta_k']    = 'Request Confirmation';
$lang['tidakada']    = 'No Confirmation Details';
$lang['terkonfirmasi']    = 'Confirmed';
$lang['transaksi_batal']    = 'Your Transaction Has Been Canceled';
$lang['belum_terkonfirmasi']    = 'Not Confirmed';
$lang['konfirmasi_t']    = 'Transaction Confirmation';
$lang['tagihan']    = 'Bill To';
$lang['umum']    = 'GENERAL';
$lang['nama']    = 'Name';
$lang['kode_pos']    = 'Postal Code';
$lang['produk_beli']    = 'Purchased Products';
$lang['qty']    = 'Qty';
$lang['total']    = 'Total';
$lang['noresi']    = 'No Receipt';
$lang['belum_resi']    = ' No Receipt Number Yet';
$lang['masih_tersedia']    = 'Still Available';
$lang['produk_h']    = 'Deleted Products';
$lang['unit']    = 'Unit';
$lang['pembayaran']    = 'Pembayaran';
$lang['informasi']    = 'Other Information';
$lang['total_t']    = 'Total Transactions';
$lang['total_p']    = 'Total Products';
$lang['catatan']    = 'Note';

// error
$lang['opps_a']    = 'Oops! The Page You Are Looking For Was Not Found';
$lang['opps_b']    = "Sorry The Page You're Looking For We Didn't Find";
$lang['opps_c']    = 'Homepage';
 ?>