<?php
$lang['bahasa']      = 'BAHASA';
$lang['alamat']      = 'Jl Smea No.4,Kota Surabaya, Jawa Timur, Indonesia';
$lang['kata']        = 'Kembangkan Bakat Dan Prestasimu Bersama Kami';
$lang['kata1']       = "Ayo";
$lang['kata2']       = "BERGABUNGLAH DENGAN KAMI !";
$lang['kata3']       = "Hak Cipta";
$lang['detik']       = "Detik";
$lang['jam']         = "Jam";
$lang['menit']       = "Menit";
// home index
$lang['eks']       	 = "Ekstrakurikular";
$lang['pres']        = "Prestasi";
$lang['pris']        = "Peristiwa";
$lang['stu']       	 = "Siswa";

$lang['home']        = 'Beranda';
$lang['aboutus']  	 = 'Tentang Kami';
$lang['about']   	 = 'Tentang Zafer';
$lang['article']	 = 'Artikel';
$lang['article_t']	 = 'Artikel Terkait';
$lang['ourArticle']	 = 'Artikel Kami';
$lang['product']	 = 'Produk';
$lang['ourProduct']	 = 'Produk Kami';
$lang['gallery']     = 'Galeri';
$lang['ourGallery']  = 'Galeri Kami';
$lang['video']       = 'Video';
$lang['picture']     = 'Gambar';
$lang['contact']     = 'Hubungi Kami';
$lang['contactVia']  = 'Silahkan Kontak Kami Via:';
$lang['read_more']   = 'Baca Lebih Lanjut';
$lang['see_more']    = 'Selengkapnya';
$lang['more']        = 'Lihat Lebih Banyak';
$lang['detail']      = 'Detail';
$lang['category']    = 'Kategori';
$lang['recent']      = 'Postingan Terbaru';
$lang['testimoni']      = 'Testimoni';
$lang['visi']      = 'Visi';
$lang['misi']      = ' Misi';
//  Cart Page
$lang['cart']        = 'Keranjang';
$lang['preorder']        = 'Pesan Terlebih Dahulu';
$lang['yourCart']    = 'Keranjang Belanja Anda';
$lang['shopNow']     = 'Lanjut Belanja';
$lang['clearAll']    = 'Kosongkan';
$lang['clear']       = 'Hapus Dari Keranjang';
$lang['productN']    = 'Nama Produk';
$lang['price']       = 'Harga';
$lang['action']      = 'Aksi';
$lang['productT']    = 'Total Barang Yang Di Beli';
$lang['booking']     = 'pemesanan';
$lang['day']         = 'Hari';
$lang['aPurchase']   = 'Setelah Pembelian';

//    Contact Page
$lang['criticsm']    = 'Kirim Kritik dan Saran';
$lang['cName']       = 'Nama Lengkap';
$lang['cEmail']      = 'Email anda';
$lang['cPhone']      = 'No Telfon anda';
$lang['cMessage']    = 'Deskripsi';
$lang['send']		 =	'Kirim';
$lang['contact']	 =	'Hubungi';
$lang['our']	 =	'Kami';
$lang['os']	 =	'Toko';
$lang['us']	 =	'Kami';
//    Custom Page
$lang['csExplain']   = 'Apabila anda ingin membuat model jaket kustom dengan desain anda sendiri, silahkan mengisi form dibawah ini:';
$lang['csName']      = 'Nama Lengkap';
$lang['csName_2']    = 'Masukkan Nama Anda';
$lang['csMale']      = 'Laki - laki';
$lang['csFemale']    = 'Perempuan';
$lang['csTitle']     = 'Data Pesanan Kustom';
$lang['csImage']     = 'Gambar Pesanan';
$lang['csAddress']   = 'Alamat';
$lang['csAddress_2'] = 'Masukkan ALamat anda';
$lang['csPhone']     = 'No Telp';
$lang['csPhone_2']   = 'Masukkan No Telp anda';
$lang['csEmail']     = 'Email';
$lang['csEmail_2']   = 'Masukkan Email anda';
$lang['csDesc']      = 'Deskripsi Pesanan';
$lang['csDesc_2']    = 'Masukkan Deskripsi Pesanan';
$lang['csGender']    = 'Jenis Kelamin';
$lang['send']    = 'Kirim';
$lang['create']    = 'BUAT MILIKMU SENDIRI';
//$lang['']    = '';
// footer
$lang['bantuan']    = 'Bantuan';
$lang['fu']='Ikuti Kami';
//product detail
 $lang['desc']    = 'Deskripsi';
$lang['col']    = 'Warna';
$lang['size']    = 'Ukuran';
// checkout sukses
$lang['thanks_a']    = 'Thank You for Shopping with Us';
$lang['thanks_b']    = 'Kami Sangat berterima kasih pada anda karena sudah berbelanja dan mempercayai kami sebagai penjual , Transaksi anda sedang kami proses , 
            	silahkan check kotak masuk / spam pada email anda untuk mengetahui langkah selanjutnya , 
            	anda belum mendapatkan email dari kami ? ';
$lang['thanks_c']    = 'Kirim Ulang Email';
$lang['thanks_d']    = 'Anda dapat melihat / mengecek status transaksi anda di halaman berikut';
$lang['thanks_e']    = 'Lacak Transaksi';
$lang['thanks_f']    = 'Jika ada hal yang ingin di tanyakan silahkan';
$lang['status']    = 'Status Transaksi';
$lang['order']    = 'MEMESAN';
$lang['request']    = 'PERMINTAAN';
$lang['approve']    = 'MENYETUJUI';
$lang['packing']    = 'PENGEPAKAN';
$lang['delivery']    = 'PENGIRIMAN';
$lang['done']    = 'DIBUAT';
$lang['cancel']    = 'MEMBATALKAN';
$lang['nomor_i']    = 'Nomor Tagihan';
$lang['tanggal']    = 'Tanggal';
$lang['kadaluarsa']    = 'Kadaluarsa';
$lang['status_a']    = 'Status Pembayaran';
$lang['meminta_k']    = 'Meminta Konfirmasi';
$lang['tidakada']    = 'Tidak Ada Detail Konfirmasi';
$lang['terkonfirmasi']    = 'Terkonfirmasi';
$lang['transaksi_batal']    = 'Transaksi Anda Telah Dibatalkan';
$lang['belum_terkonfirmasi']    = 'Belum Terkonfirmasi';
$lang['konfirmasi_t']    = 'Konfirmasi Transaksi';
$lang['tagihan']    = 'Tagihan Kepada';
$lang['umum']    = 'UMUM';
$lang['nama']    = 'Nama';
$lang['kode_pos']    = 'Kode Pos';
$lang['produk_beli']    = 'Produk Yang Dibeli';
$lang['qty']    = 'Jumlah';
$lang['total']    = 'Total';
$lang['noresi']    = 'No Resi';
$lang['belum_resi']    = ' Belum Ada Nomor Resi';
$lang['masih_tersedia']    = 'Masih Tersedia';
$lang['produk_h']    = 'Produk Terhapus';
$lang['unit']    = 'Satuan';
$lang['pembayaran']    = 'Pembayaran';
$lang['informasi']    = 'Informasi Lainnya';
$lang['total_t']    = 'Total Transaksi';
$lang['total_p']    = 'Total Produk';
$lang['catatan']    = 'Catatan';

// error
$lang['opps_a']    = 'Oops! Halaman Yang Anda Cari Tidak Ditemukan';
$lang['opps_b']    = 'Maaf Halaman Yang Anda cari Tidak Kami Temukan';
$lang['opps_c']    = 'Halaman Awal';

 ?>